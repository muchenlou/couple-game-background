package com.ruoyi.quartz.task;

import cn.hutool.core.lang.Console;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.demo.domain.FiveBasics;
import com.ruoyi.demo.domain.FiveUser;
import com.ruoyi.demo.mapper.FiveUserMapper;
import com.ruoyi.demo.service.IFiveBasicsService;
import com.ruoyi.demo.service.IFiveUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.math.BigDecimal;
import java.util.List;
import java.util.Random;

/**
 * 定时任务调度测试
 * 
 * @author ruoyi
 */
@Component("ryTask")
public class RyTask
{

    @Autowired
    private  IFiveUserService iFiveUserService;
    @Autowired
    private  IFiveBasicsService iFiveBasicsService;
    @Autowired
    private FiveUserMapper fiveUserMapper;
    @Autowired
    private IFiveBasicsService fiveBasicsService;
    public void ryMultipleParams()
    {
        fiveUserMapper.updatewxb();
        fiveUserMapper.updatecloudw();

        Console.log(StrUtil.format("红利计算" ));
    }

    public void ryParams()
    {
        fiveUserMapper.updatesign();
    }

    public void ryNoParams() {

    }
}
