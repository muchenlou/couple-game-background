import request from '@/utils/request'

// 查询公众号订单列表
export function listOrder(query) {
  return request({
    url: '/qinglv/order/list',
    method: 'get',
    params: query
  })
}

// 查询公众号订单详细
export function getOrder(id) {
  return request({
    url: '/qinglv/order/' + id,
    method: 'get'
  })
}

// 新增公众号订单
export function addOrder(data) {
  return request({
    url: '/qinglv/order',
    method: 'post',
    data: data
  })
}

// 修改公众号订单
export function updateOrder(data) {
  return request({
    url: '/qinglv/order',
    method: 'put',
    data: data
  })
}

// 删除公众号订单
export function delOrder(id) {
  return request({
    url: '/qinglv/order/' + id,
    method: 'delete'
  })
}

// 导出公众号订单
export function exportOrder(query) {
  return request({
    url: '/qinglv/order/export',
    method: 'get',
    params: query
  })
}