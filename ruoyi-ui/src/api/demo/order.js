import request from '@/utils/request'

// 查询order列表
export function listOrder(query) {
  return request({
    url: '/demo/order/list',
    method: 'get',
    params: query
  })
}

// 查询order详细
export function getOrder(id) {
  return request({
    url: '/demo/order/' + id,
    method: 'get'
  })
}

// 新增order
export function addOrder(data) {
  return request({
    url: '/demo/order',
    method: 'post',
    data: data
  })
}

// 修改order
export function updateOrder(data) {
  return request({
    url: '/demo/order',
    method: 'put',
    data: data
  })
}

// 删除order
export function delOrder(id) {
  return request({
    url: '/demo/order/' + id,
    method: 'delete'
  })
}

// 导出order
export function exportOrder(query) {
  return request({
    url: '/demo/order/export',
    method: 'get',
    params: query
  })
}