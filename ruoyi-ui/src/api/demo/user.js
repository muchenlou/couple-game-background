import request from '@/utils/request'

// 查询user列表
export function listUser(query) {
  return request({
    url: '/demo/user/list',
    method: 'get',
    params: query
  })
}

// 查询user详细
export function getUser(id) {
  return request({
    url: '/demo/user/' + id,
    method: 'get'
  })
}

// 新增user
export function addUser(data) {
  return request({
    url: '/demo/user',
    method: 'post',
    data: data
  })
}

// 修改user
export function updateUser(data) {
  return request({
    url: '/demo/user',
    method: 'put',
    data: data
  })
}

// 删除user
export function delUser(id) {
  return request({
    url: '/demo/user/' + id,
    method: 'delete'
  })
}

// 导出user
export function exportUser(query) {
  return request({
    url: '/demo/user/export',
    method: 'get',
    params: query
  })
}