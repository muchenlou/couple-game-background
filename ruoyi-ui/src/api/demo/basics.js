import request from '@/utils/request'

// 查询basics列表
export function listBasics(query) {
  return request({
    url: '/demo/basics/list',
    method: 'get',
    params: query
  })
}

// 查询basics详细
export function getBasics(id) {
  return request({
    url: '/demo/basics/' + id,
    method: 'get'
  })
}

// 新增basics
export function addBasics(data) {
  return request({
    url: '/demo/basics',
    method: 'post',
    data: data
  })
}

// 修改basics
export function updateBasics(data) {
  return request({
    url: '/demo/basics',
    method: 'put',
    data: data
  })
}

// 删除basics
export function delBasics(id) {
  return request({
    url: '/demo/basics/' + id,
    method: 'delete'
  })
}

// 导出basics
export function exportBasics(query) {
  return request({
    url: '/demo/basics/export',
    method: 'get',
    params: query
  })
}