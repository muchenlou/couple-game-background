package com.ruoyi.qinglv.vo;

import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;



/**
 * 公众号订单视图对象 mall_package
 *
 * @author ruoyi
 * @date 2023-10-25
 */
@Data
@ApiModel("公众号订单视图对象")
public class TbOrderVo {
	private static final long serialVersionUID = 1L;

	/** $pkColumn.columnComment */
	@ApiModelProperty("$pkColumn.columnComment")
	private Long id;

	/** 订单编号 */
	@Excel(name = "订单编号")
	@ApiModelProperty("订单编号")
	private String orderNumber;
	/** 订单id */
	@Excel(name = "订单id")
	@ApiModelProperty("订单id")
	private String orderId;
	/** 订单金额 */
	@Excel(name = "订单金额")
	@ApiModelProperty("订单金额")
	private String orderPrice;
	/** 微信用户id */
	@Excel(name = "微信用户id")
	@ApiModelProperty("微信用户id")
	private String openId;
	/** 代理商名称 */
	@Excel(name = "代理商名称")
	@ApiModelProperty("代理商名称")
	private String dailiName;
	/** 代理商ID */
	@Excel(name = "代理商ID")
	@ApiModelProperty("代理商ID")
	private String daliId;
	/** 付款成功时间 */
	@Excel(name = "付款成功时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("付款成功时间")
	private Date payTime;
	/** 0-预支付 1-已支付 2-取消或者支付失败 */
	@Excel(name = "0-预支付 1-已支付 2-取消或者支付失败")
	@ApiModelProperty("0-预支付 1-已支付 2-取消或者支付失败")
	private Integer status;
	/** 付款成功时间 */
	@Excel(name = "订单创建时间" , width = 30, dateFormat = "yyyy-MM-dd")
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("付款成功时间")
	private Date createTime;

}
