package com.ruoyi.qinglv.controller;

import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.qinglv.bo.TbOrderAddBo;
import com.ruoyi.qinglv.service.ITbOrderService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @Author Baojian Hong
 * @Date 2023/10/25 13:37
 * @Version 1.0
 */

@RestController
@RequestMapping("/remote/order/v1")
@Api(value = "远程订单添加", tags = {"远程订单添加"})
public class RemoteOrderController extends BaseController {

    @Autowired
    private  ITbOrderService iTbOrderService;

    @PostMapping("/addOrder")
    @ApiOperation("远程添加订单接口")
    public AjaxResult addOrder(@RequestBody TbOrderAddBo tbOrderAddBo){
        iTbOrderService.remoteAddOrder(tbOrderAddBo);
        return AjaxResult.success();
    }

    @PostMapping("/updateOrder")
    @ApiOperation("远程修改订单接口")
    public AjaxResult updateOrder(@RequestBody TbOrderAddBo tbOrderAddBo){
        iTbOrderService.remoteUpdateOrder(tbOrderAddBo);
        return AjaxResult.success();
    }
}
