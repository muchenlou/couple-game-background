package com.ruoyi.qinglv.controller;

import java.util.List;
import java.util.Arrays;

import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.qinglv.vo.TbOrderVo;
import com.ruoyi.qinglv.bo.TbOrderQueryBo;
import com.ruoyi.qinglv.bo.TbOrderAddBo;
import com.ruoyi.qinglv.bo.TbOrderEditBo;
import com.ruoyi.qinglv.service.ITbOrderService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * 公众号订单Controller
 * 
 * @author ruoyi
 * @date 2023-10-25
 */
@Api(value = "公众号订单控制器", tags = {"公众号订单管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/qinglv/order")
public class TbOrderController extends BaseController {

    private final ITbOrderService iTbOrderService;

    /**
     * 查询公众号订单列表
     */
    @ApiOperation("查询公众号订单列表")
    @PreAuthorize("@ss.hasPermi('qinglv:order:list')")
    @GetMapping("/list")
    public TableDataInfo<TbOrderVo> list(TbOrderQueryBo bo) {
        startPage();
        List<TbOrderVo> list = iTbOrderService.queryList(bo);
        return getDataTable(list);
    }

    /**
     * 导出公众号订单列表
     */
    @ApiOperation("导出公众号订单列表")
    @PreAuthorize("@ss.hasPermi('qinglv:order:export')")
    @Log(title = "公众号订单", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult<TbOrderVo> export(TbOrderQueryBo bo) {
        List<TbOrderVo> list = iTbOrderService.queryList(bo);
        ExcelUtil<TbOrderVo> util = new ExcelUtil<TbOrderVo>(TbOrderVo.class);
        return util.exportExcel(list, "公众号订单");
    }

    /**
     * 获取公众号订单详细信息
     */
    @ApiOperation("获取公众号订单详细信息")
    @PreAuthorize("@ss.hasPermi('qinglv:order:query')")
    @GetMapping("/{id}")
    public AjaxResult<TbOrderVo> getInfo(@PathVariable("id" ) Long id) {
        return AjaxResult.success(iTbOrderService.queryById(id));
    }

    /**
     * 新增公众号订单
     */
    @ApiOperation("新增公众号订单")
    @PreAuthorize("@ss.hasPermi('qinglv:order:add')")
    @Log(title = "公众号订单", businessType = BusinessType.INSERT)
    @PostMapping()
    public AjaxResult<Void> add(@RequestBody TbOrderAddBo bo) {
        return toAjax(iTbOrderService.insertByAddBo(bo) ? 1 : 0);
    }

    /**
     * 修改公众号订单
     */
    @ApiOperation("修改公众号订单")
    @PreAuthorize("@ss.hasPermi('qinglv:order:edit')")
    @Log(title = "公众号订单", businessType = BusinessType.UPDATE)
    @PutMapping()
    public AjaxResult<Void> edit(@RequestBody TbOrderEditBo bo) {
        return toAjax(iTbOrderService.updateByEditBo(bo) ? 1 : 0);
    }

    /**
     * 删除公众号订单
     */
    @ApiOperation("删除公众号订单")
    @PreAuthorize("@ss.hasPermi('qinglv:order:remove')")
    @Log(title = "公众号订单" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult<Void> remove(@PathVariable Long[] ids) {
        return toAjax(iTbOrderService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }

}
