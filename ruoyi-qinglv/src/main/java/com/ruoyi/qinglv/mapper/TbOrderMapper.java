package com.ruoyi.qinglv.mapper;

import com.ruoyi.qinglv.domain.TbOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * 公众号订单Mapper接口
 *
 * @author ruoyi
 * @date 2023-10-25
 */
public interface TbOrderMapper extends BaseMapper<TbOrder> {

}
