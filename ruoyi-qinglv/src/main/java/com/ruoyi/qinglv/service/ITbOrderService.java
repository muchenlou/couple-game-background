package com.ruoyi.qinglv.service;

import com.ruoyi.qinglv.domain.TbOrder;
import com.ruoyi.qinglv.vo.TbOrderVo;
import com.ruoyi.qinglv.bo.TbOrderQueryBo;
import com.ruoyi.qinglv.bo.TbOrderAddBo;
import com.ruoyi.qinglv.bo.TbOrderEditBo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Collection;
import java.util.List;

/**
 * 公众号订单Service接口
 *
 * @author ruoyi
 * @date 2023-10-25
 */
public interface ITbOrderService extends IService<TbOrder> {
	/**
	 * 查询单个
	 * @return
	 */
	TbOrderVo queryById(Long id);

	/**
	 * 查询列表
	 */
	List<TbOrderVo> queryList(TbOrderQueryBo bo);

	/**
	 * 根据新增业务对象插入公众号订单
	 * @param bo 公众号订单新增业务对象
	 * @return
	 */
	Boolean insertByAddBo(TbOrderAddBo bo);

	/**
	 * 根据编辑业务对象修改公众号订单
	 * @param bo 公众号订单编辑业务对象
	 * @return
	 */
	Boolean updateByEditBo(TbOrderEditBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

	void remoteAddOrder(TbOrderAddBo tbOrderAddBo);

	void remoteUpdateOrder(TbOrderAddBo tbOrderAddBo);
}
