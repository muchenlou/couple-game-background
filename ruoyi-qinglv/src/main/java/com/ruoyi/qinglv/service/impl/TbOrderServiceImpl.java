package com.ruoyi.qinglv.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.apache.commons.lang3.StringUtils;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.pagehelper.Page;
import com.ruoyi.qinglv.bo.TbOrderAddBo;
import com.ruoyi.qinglv.bo.TbOrderQueryBo;
import com.ruoyi.qinglv.bo.TbOrderEditBo;
import com.ruoyi.qinglv.domain.TbOrder;
import com.ruoyi.qinglv.mapper.TbOrderMapper;
import com.ruoyi.qinglv.vo.TbOrderVo;
import com.ruoyi.qinglv.service.ITbOrderService;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * 公众号订单Service业务层处理
 *
 * @author ruoyi
 * @date 2023-10-25
 */
@Service
public class TbOrderServiceImpl extends ServiceImpl<TbOrderMapper, TbOrder> implements ITbOrderService {

    @Override
    public TbOrderVo queryById(Long id){
        TbOrder db = this.baseMapper.selectById(id);
        return BeanUtil.toBean(db, TbOrderVo.class);
    }

    @Override
    public List<TbOrderVo> queryList(TbOrderQueryBo bo) {
        LambdaQueryWrapper<TbOrder> lqw = Wrappers.lambdaQuery();
        lqw.eq(StrUtil.isNotBlank(bo.getOrderNumber()), TbOrder::getOrderNumber, bo.getOrderNumber());
        lqw.eq(StrUtil.isNotBlank(bo.getOrderId()), TbOrder::getOrderId, bo.getOrderId());
        lqw.eq(StrUtil.isNotBlank(bo.getOrderPrice()), TbOrder::getOrderPrice, bo.getOrderPrice());
        lqw.eq(StrUtil.isNotBlank(bo.getOpenId()), TbOrder::getOpenId, bo.getOpenId());
        lqw.like(StrUtil.isNotBlank(bo.getDailiName()), TbOrder::getDailiName, bo.getDailiName());
        lqw.eq(StrUtil.isNotBlank(bo.getDaliId()), TbOrder::getDaliId, bo.getDaliId());
        lqw.eq(bo.getPayTime() != null, TbOrder::getPayTime, bo.getPayTime());
        lqw.eq(bo.getStatus() != null, TbOrder::getStatus, bo.getStatus());
        lqw.orderByDesc(TbOrder::getCreateTime);
        return entity2Vo(this.list(lqw));
    }

    /**
    * 实体类转化成视图对象
    *
    * @param collection 实体类集合
    * @return
    */
    private List<TbOrderVo> entity2Vo(Collection<TbOrder> collection) {
        List<TbOrderVo> voList = collection.stream()
                .map(any -> BeanUtil.toBean(any, TbOrderVo.class))
                .collect(Collectors.toList());
        if (collection instanceof Page) {
            Page<TbOrder> page = (Page<TbOrder>)collection;
            Page<TbOrderVo> pageVo = new Page<>();
            BeanUtil.copyProperties(page,pageVo);
            pageVo.addAll(voList);
            voList = pageVo;
        }
        return voList;
    }

    @Override
    public Boolean insertByAddBo(TbOrderAddBo bo) {
        TbOrder add = BeanUtil.toBean(bo, TbOrder.class);
        validEntityBeforeSave(add);
        return this.save(add);
    }

    @Override
    public Boolean updateByEditBo(TbOrderEditBo bo) {
        TbOrder update = BeanUtil.toBean(bo, TbOrder.class);
        validEntityBeforeSave(update);
        return this.updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(TbOrder entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return this.removeByIds(ids);
    }

    @Override
    public void remoteAddOrder(TbOrderAddBo tbOrderAddBo) {
        TbOrder add = BeanUtil.toBean(tbOrderAddBo, TbOrder.class);
        this.save(add);
    }

    @Override
    public void remoteUpdateOrder(TbOrderAddBo tbOrderAddBo) {
        if (StringUtils.isEmpty(tbOrderAddBo.getOrderId())){
            return;
        }
        TbOrder order = this.getOne(new QueryWrapper<TbOrder>().eq("order_id", tbOrderAddBo.getOrderId()));
        order.setStatus(tbOrderAddBo.getStatus());
        order.setPayTime(tbOrderAddBo.getPayTime());
        this.updateById(order);
    }
}
