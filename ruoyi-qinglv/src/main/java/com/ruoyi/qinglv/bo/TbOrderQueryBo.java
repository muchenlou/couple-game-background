package com.ruoyi.qinglv.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.Map;
import java.util.HashMap;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 公众号订单分页查询对象 tb_order
 *
 * @author ruoyi
 * @date 2023-10-25
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("公众号订单分页查询对象")
public class TbOrderQueryBo extends BaseEntity {

	/** 分页大小 */
	@ApiModelProperty("分页大小")
	private Integer pageSize;
	/** 当前页数 */
	@ApiModelProperty("当前页数")
	private Integer pageNum;
	/** 排序列 */
	@ApiModelProperty("排序列")
	private String orderByColumn;
	/** 排序的方向desc或者asc */
	@ApiModelProperty(value = "排序的方向", example = "asc,desc")
	private String isAsc;


	/** 订单编号 */
	@ApiModelProperty("订单编号")
	private String orderNumber;
	/** 订单id */
	@ApiModelProperty("订单id")
	private String orderId;
	/** 订单金额 */
	@ApiModelProperty("订单金额")
	private String orderPrice;
	/** 微信用户id */
	@ApiModelProperty("微信用户id")
	private String openId;
	/** 代理商名称 */
	@ApiModelProperty("代理商名称")
	private String dailiName;
	/** 代理商ID */
	@ApiModelProperty("代理商ID")
	private String daliId;
	/** 付款成功时间 */
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@ApiModelProperty("付款成功时间")
	private Date payTime;
	/** 0-预支付 1-已支付 2-取消或者支付失败 */
	@ApiModelProperty("0-预支付 1-已支付 2-取消或者支付失败")
	private Integer status;

}
