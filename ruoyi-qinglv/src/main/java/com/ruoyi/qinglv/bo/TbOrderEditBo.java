package com.ruoyi.qinglv.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;

import java.util.Date;
import com.fasterxml.jackson.annotation.JsonFormat;

/**
 * 公众号订单编辑对象 tb_order
 *
 * @author ruoyi
 * @date 2023-10-25
 */
@Data
@ApiModel("公众号订单编辑对象")
public class TbOrderEditBo {


    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private Long id;

    /** 订单编号 */
    @ApiModelProperty("订单编号")
    private String orderNumber;

    /** 订单id */
    @ApiModelProperty("订单id")
    private String orderId;

    /** 订单金额 */
    @ApiModelProperty("订单金额")
    private String orderPrice;

    /** 微信用户id */
    @ApiModelProperty("微信用户id")
    private String openId;

    /** 代理商名称 */
    @ApiModelProperty("代理商名称")
    private String dailiName;

    /** 代理商ID */
    @ApiModelProperty("代理商ID")
    private String daliId;

    /** 付款成功时间 */
    @ApiModelProperty("付款成功时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date payTime;

    /** 0-预支付 1-已支付 2-取消或者支付失败 */
    @ApiModelProperty("0-预支付 1-已支付 2-取消或者支付失败")
    private Integer status;
}
