package com.ruoyi.qinglv.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import com.ruoyi.common.annotation.Excel;

/**
 * 公众号订单对象 tb_order
 * 
 * @author ruoyi
 * @date 2023-10-25
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("tb_order")
public class TbOrder implements Serializable {

private static final long serialVersionUID=1L;


    /** $column.columnComment */
    @TableId(value = "id")
    private Long id;

    /** 订单编号 */
    private String orderNumber;

    /** 订单id */
    private String orderId;

    /** 订单金额 */
    private String orderPrice;

    /** 微信用户id */
    private String openId;

    /** 代理商名称 */
    private String dailiName;

    /** 代理商ID */
    private String daliId;

    /** 付款时间 */
//    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** 付款成功时间 */
    private Date payTime;

    /** 0-预支付 1-已支付 2-取消或者支付失败 */
    private Integer status;

}
