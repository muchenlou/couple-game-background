package com.ruoyi.demo.vo;

import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;



/**
 * order视图对象 mall_package
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Data
@ApiModel("order视图对象")
public class FiveOrderVo {
	private static final long serialVersionUID = 1L;

	/** $pkColumn.columnComment */
	@ApiModelProperty("$pkColumn.columnComment")
	private Long id;

	/** $column.columnComment */
	@Excel(name = "${comment}" , readConverterExp = "$column.readConverterExp()")
	@ApiModelProperty("$column.columnComment")
	private Long uid;
	/** 单号 */
	@Excel(name = "单号")
	@ApiModelProperty("单号")
	private String number;
	/** 支付状态 */
	@Excel(name = "支付状态")
	@ApiModelProperty("支付状态")
	private String code;
	/** $column.columnComment */
	@Excel(name = "支付状态")
	@ApiModelProperty("$column.columnComment")
	private Long amount;

	private Integer grade ;

}
