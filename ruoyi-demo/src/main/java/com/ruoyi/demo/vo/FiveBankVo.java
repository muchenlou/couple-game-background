package com.ruoyi.demo.vo;

import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;



/**
 * bank视图对象 mall_package
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Data
@ApiModel("bank视图对象")
public class FiveBankVo {
	private static final long serialVersionUID = 1L;

	/** $pkColumn.columnComment */
	@ApiModelProperty("$pkColumn.columnComment")
	private Long id;

	/** $column.columnComment */
	@Excel(name = "${comment}" , readConverterExp = "$column.readConverterExp()")
	@ApiModelProperty("$column.columnComment")
	private String name;
	/** $column.columnComment */
	@Excel(name = "${comment}" , readConverterExp = "$column.readConverterExp()")
	@ApiModelProperty("$column.columnComment")
	private String identity;
	/** $column.columnComment */
	@Excel(name = "${comment}" , readConverterExp = "$column.readConverterExp()")
	@ApiModelProperty("$column.columnComment")
	private String bankCard;
	/** $column.columnComment */
	@Excel(name = "${comment}" , readConverterExp = "$column.readConverterExp()")
	@ApiModelProperty("$column.columnComment")
	private String bank;
	/** $column.columnComment */
	@Excel(name = "${comment}" , readConverterExp = "$column.readConverterExp()")
	@ApiModelProperty("$column.columnComment")
	private String status;

	private String  tpwd;

}
