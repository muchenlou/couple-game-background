package com.ruoyi.demo.vo;

import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import java.math.BigDecimal;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;



/**
 * record视图对象 mall_package
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Data
@ApiModel("record视图对象")
public class FiveRecordVo {
	private static final long serialVersionUID = 1L;

	/** id */
	@ApiModelProperty("id")
	private Long id;

	/** 用户id */
	@Excel(name = "用户id")
	@ApiModelProperty("用户id")
	private Long uid;
	/** 金额 */
	@Excel(name = "金额")
	@ApiModelProperty("金额")
	private BigDecimal amount;
	/** 1余额2五连3wxb4黄金 */
	@Excel(name = "1余额2五连3wxb4黄金")
	@ApiModelProperty("1余额2五连3wxb4黄金")
	private Integer  coin;
	/**  1注册  2邀请 */
	@Excel(name = " 1注册  2邀请")
	@ApiModelProperty(" 1注册  2邀请")
	private Integer  event;

}
