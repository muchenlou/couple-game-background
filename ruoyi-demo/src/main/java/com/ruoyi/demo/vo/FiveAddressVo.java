package com.ruoyi.demo.vo;

import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;



/**
 * address视图对象 mall_package
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Data
@ApiModel("address视图对象")
public class FiveAddressVo {
	private static final long serialVersionUID = 1L;

	/** $pkColumn.columnComment */
	@ApiModelProperty("$pkColumn.columnComment")
	private Long id;

	/** $column.columnComment */
	@Excel(name = "${comment}" , readConverterExp = "$column.readConverterExp()")
	@ApiModelProperty("$column.columnComment")
	private Long uid;
	/** $column.columnComment */
	@Excel(name = "${comment}" , readConverterExp = "$column.readConverterExp()")
	@ApiModelProperty("$column.columnComment")
	private String address;
	/** $column.columnComment */
	@Excel(name = "${comment}" , readConverterExp = "$column.readConverterExp()")
	@ApiModelProperty("$column.columnComment")
	private String phone;
	/** $column.columnComment */
	@Excel(name = "${comment}" , readConverterExp = "$column.readConverterExp()")
	@ApiModelProperty("$column.columnComment")
	private String name;
	/** $column.columnComment */
	@Excel(name = "${comment}" , readConverterExp = "$column.readConverterExp()")
	@ApiModelProperty("$column.columnComment")
	private Long status;
	private String createTime;
}
