package com.ruoyi.demo.vo;

import com.ruoyi.common.annotation.Excel;
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import java.util.Date;



/**
 * 用户视图对象 mall_package
 *
 * @author ruoyi
 * @date 2021-12-09
 */
@Data
@ApiModel("用户视图对象")
public class FiveUserVo {
	private static final long serialVersionUID = 1L;

	/** id */
	@ApiModelProperty("id")
	private Long id;

	/** 手机号 */
	@Excel(name = "手机号")
	@ApiModelProperty("手机号")
	private String phone;
	/** 0普通会员  1.会员 */
	@Excel(name = "0普通会员  1.会员")
	@ApiModelProperty("0普通会员  1.会员")
	private Long identity;

	/** 0普通会员  1.会员 */
	private Integer identityx;
	/** 0普通会员  1.会员 */
	private Integer identityg;
	/** 余额 */
	@Excel(name = "余额")
	@ApiModelProperty("余额")
	private Integer balance;
	/** wxb */
	@Excel(name = "wxb")
	@ApiModelProperty("wxb")
	private Integer wxb;
	/**  云数五联币 */
	@Excel(name = " 云数五联币")
	@ApiModelProperty(" 云数五联币")
	private Long cloudw;
	/** 云数黄金 */
	@Excel(name = "云数黄金")
	@ApiModelProperty("云数黄金")
	private Long cloudh;
	/** 邀请码 */
	@Excel(name = "邀请码")
	@ApiModelProperty("邀请码")
	private String code;

	private String	locations;
	/** 股权 */
	@Excel(name = "股权")
	@ApiModelProperty("股权")
	private Integer stockRight;
	/** 邀请人id */
	@Excel(name = "邀请人id")
	@ApiModelProperty("邀请人id")
	private Long invitationId;
	/** 状态 */
	@Excel(name = "状态")
	@ApiModelProperty("状态")
	private Long status;
	/** 二维码 */
	@Excel(name = "二维码")
	@ApiModelProperty("二维码")
	private String img;
	@JsonFormat(pattern = "yyyy-MM-dd hh:mm:ss",timezone = "GMT+8")
	private Date createTime;

	@Excel(name = "签到")
	@ApiModelProperty("签到")
	private Integer signIn;

	private  Integer  signicon;
}
