package com.ruoyi.demo.file;

public enum ErrorEnum {
    SUCCESS(0, "success"),
    FAIL(-1, "error"),
    ERROR_SERVER(50000, "服务异常,请稍后重试"),
    UPLOAD_ERROR(10016, "文件上传失败！"),
    USER_NOT_EXITES(401000,"用户不存在"),
    USER_AUTH_FAILED(401001, "用户授权失败"),
    USER_INFO_ERROR(401004,"用户名或密码错误"),
    SIGN_VERIFI_ERROR(401002,"token解析错误"),
    SIGN_VERIFI_EXPIRE(401003,"token已过期"),
    MANAGER_NOT_ROLE(401005,"没有此权限"),
    INVALID_PARAM(400001, "非法参数"),
    USER_EXITES(100001,"用户已存在"),
    USER_PASSWORDERROR(100008,"密码错误"),
    USER_ISSTATUS(100007,"该账户已被冻结"),
    USER_PHONE(100017,"该手机号已存在"),
    USER_ADMINNAME(100016,"该登录名已存在"),
    USER_IDENTITY(100015,"该身份证已存在"),
    USER_IDENTITYNOT(100014,"身份证格式不正确"),
    USER_ISNULL(100081,"收件人信息为空"),
    shool_ISNULL(100070,"已存在该学校"),
    grade_ISNULL(100071,"学校已存在该年级"),
    clas_ISNULL(100072,"年级已存在该班级"),
    number_ISNULL(100073,"学号已存在"),
    is_ISNULL(100060,"添加失败"),
    is_UPDATE(100061,"修改失败"),
    is_DEL(100062,"删除失败"),
    text_ISNOTNULL(100051,"该课程已有习题"),
    GOOD_COURE(100041,"已点赞"),
    GOOD_NOT(100031,"点赞失败"),
    COURE_GET(100021,"已购买不能再次购买"),
    VIP_GET(100011,"会员购买异常"),
    BUY_CU(100012,"购买异常"),
    STUTEND_ISNOT(100013,"未查到学生信息"),
    is_BUY(100014,"已购买"),
    is_NOTCLASS(100015,"不存在该班级")


    ;


    private Integer code;
    private String message;

    ErrorEnum(Integer code, String message){
        this.code = code;
        this.message = message;
    }


    public Integer getCode() {
        return code;
    }
    public String getMessage() {
        return message;
    }
}
