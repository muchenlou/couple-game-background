package com.ruoyi.demo.controller;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.aliyuncs.http.HttpRequest;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.github.binarywang.wxpay.bean.notify.WxPayNotifyResponse;
import com.github.binarywang.wxpay.exception.WxPayException;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.demo.domain.FiveBasics;
import com.ruoyi.demo.domain.FiveOrder;
import com.ruoyi.demo.domain.FiveUser;

import com.ruoyi.demo.service.*;
import com.ruoyi.demo.util.*;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.demo.vo.FiveOrderVo;
import com.ruoyi.demo.bo.FiveOrderQueryBo;
import com.ruoyi.demo.bo.FiveOrderAddBo;
import com.ruoyi.demo.bo.FiveOrderEditBo;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

import static javafx.scene.input.KeyCode.H;

/**
 * orderController
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Api(value = "order控制器", tags = {"order管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/demo/order")
public class FiveOrderController extends BaseController {

    private final IFiveOrderService iFiveOrderService;
    @Autowired
    private  IFiveUserService iFiveUserService;
    @Autowired
    private LoginTokenService loginTokenService;
    @Autowired
    private  IFiveRecordService iFiveRecordService;

    private final IFiveBasicsService iFiveBasicsService;





    public static void main(String[] args) throws ParseException {
        String orderId = String.valueOf(UUIDUtils.randomLong());
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = new Date(System.currentTimeMillis());
        String format = df1.format(date);
        System.out.println(format);

        int i=2;
        String    mch_id="";
        String    amount ="";
        String    pass_code ="";
        String    notify_url ="";
        String    return_url ="";
        String    key ="";
        String    payUrl ="";
        if( i==1){
                mch_id="1102";
                amount ="1000";
                pass_code ="903";
                notify_url ="http://bitfil.cc/demo/demo/order/notifyUrl";
                return_url ="http://bitfil.cc/#/home/assets";
                key ="0a7ce601e20e4fefa5ad401df2c8c643";
                payUrl ="http://202.79.166.98:8888/api/unifiedorder";
        } else if (i==2){
            mch_id="1003";
            amount ="500";
            pass_code ="1002";
            notify_url ="http://bitfil.cc/demo/demo/order/notifyUrl";
            return_url ="http://bitfil.cc/#/home/assets";
            key ="e2dfb54f9723491c9041694c31b88cba";
            payUrl ="http://137.220.128.97:8888/api/unifiedorder";
        }


        Map map=new HashMap();
        map.put("mch_id",mch_id);//商务号
        map.put("out_trade_no",orderId);//商户订单号
        map.put("amount",amount);//支付金额
        map.put("pass_code",pass_code);//通道类型
        map.put("subject","hu");//商品名称
        map.put("attch","hu");//商品名称
        map.put("client_ip","103.135.34.114");//用户IP地址
        map.put("notify_url",notify_url);//异步通知地址
        map.put("return_url",return_url);//同步通知地址
        map.put("timestamp",format);//支付类型
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("mch_id",mch_id);//商务号
        jsonObject.put("out_trade_no",orderId);//商户订单号
        jsonObject.put("amount",amount);//支付金额
        jsonObject.put("pass_code",pass_code);//通道类型
        jsonObject.put("subject","hu");//商品名称
        jsonObject.put("attch","hu");//商品名称
        jsonObject.put("client_ip","103.135.34.114");//用户IP地址
        jsonObject.put("notify_url",notify_url);//异步通知地址
        jsonObject.put("return_url",return_url);//同步通知地址
        jsonObject.put("timestamp",format);//支付类型

        String stringSignTemp = Asc.formatUrlMap(map, false, false)+key;
        String sign = MD5.getMd5(stringSignTemp,32).toUpperCase();
        jsonObject.put("sign",sign);//签名
        System.out.println(sign);
        System.out.println(jsonObject);
        String wxResult = HttpClientUtil.doPostJson(payUrl,jsonObject.toJSONString());
        System.out.println(wxResult);

        JSONObject json = JSONObject.parseObject(wxResult);
                if(json.get("code")!=null&& (int)json.get("code")!=0){
                    throw new RuntimeException(json.get("msg").toString());
                }
        String data = json.get("data").toString();
        System.out.println(data);
        JSONObject jsonObject1 = JSONObject.parseObject(data);
        System.out.println(jsonObject1);
        if(jsonObject1!=null){

        }

        //JSONObject jsonObject = JSONObject.parseObject(wxResult);
    }

    /**
          * 新增order
          */
    @ApiOperation("支付")

    @Log(title = "order", businessType = BusinessType.INSERT)
    @PostMapping("/pay")
    public AjaxResult<Void> pay(@RequestBody FiveOrderAddBo bo) throws ParseException {
        Long userId = loginTokenService.getLoginUserId(ServletUtils.getRequest());
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        System.out.println(df1.format(date));



        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("uid",userId);
        queryWrapper.like("create_time",df1.format(date));
        queryWrapper.orderByDesc("create_time");
        List<FiveOrder> list = iFiveOrderService.list(queryWrapper);
        if(list.size()>0) {
            Date createTime = list.get(0).getCreateTime();
            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
            Date d1 = df.parse(df.format(createTime));

            Date d2 = df.parse(df.format(date));

            long diff = d2.getTime() - d1.getTime();

            long time = diff / (1000 * 60);
            if (time <= 5) {
                throw new RuntimeException("支付频繁，请稍后五分钟重试");
            }
        }
        if(userId==null){
            userId=bo.getUid();
        }
        if(userId==null){
            throw new RuntimeException("请重登录");
        }
        String orderId = String.valueOf(UUIDUtils.randomLong());
        String nonceStr = UUIDUtils.uuid2();
        bo.setUid(userId);
        bo.setAmount(bo.getAmount());
        bo.setCode(1);
        bo.setNumber(orderId);
        FiveBasics byId = iFiveBasicsService.getById(3);
        FiveBasics byId4 = iFiveBasicsService.getById(4);
        String uuu=null;
        String    mch_id="";
        String    pass_code =null;
        if(bo.getType()==0){
                pass_code =byId.getConent().split("-")[0];
        }
        if(bo.getType()==1){
            pass_code =byId.getConent().split("-")[1];
        }

        String    notify_url =byId4.getConent()+":8081/demo/order/notifyUrl";
        String    return_url =byId4.getConent()+"/#/home/assets";
        String    key ="";
        String    payUrl ="";
                if( byId.getTitle()==1){

                } else if (byId.getTitle()==2){
                        mch_id="1102";
                        key ="0a7ce601e20e4fefa5ad401df2c8c643";
                        payUrl ="http://202.79.166.98:8888/api/unifiedorder";
                    } else if (byId.getTitle()==3){
                        mch_id="1003";
                        key ="e2dfb54f9723491c9041694c31b88cba";
                        payUrl ="http://137.220.128.97:8888/api/unifiedorder";
                    }
                    DateFormat df2 = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
                    Date date2 = new Date(System.currentTimeMillis());
                    String format = df2.format(date2);

        Map map=new HashMap();
        map.put("mch_id",mch_id);//商务号
        map.put("out_trade_no",orderId);//商户订单号
        map.put("amount",bo.getAmount()+"");//支付金额
        map.put("pass_code",pass_code);//通道类型
        map.put("subject","hu");//商品名称
        map.put("attch","hu");//商品名称
        map.put("client_ip","103.135.34.114");//用户IP地址
        map.put("notify_url",notify_url);//异步通知地址
        map.put("return_url",return_url);//同步通知地址
        map.put("timestamp",format);//支付类型
        JSONObject jsonObject=new JSONObject();
        jsonObject.put("mch_id",mch_id);//商务号
        jsonObject.put("out_trade_no",orderId);//商户订单号
        jsonObject.put("amount",bo.getAmount()+"");//支付金额
        jsonObject.put("pass_code",pass_code);//通道类型
        jsonObject.put("subject","hu");//商品名称
        jsonObject.put("attch","hu");//商品名称
        jsonObject.put("client_ip","103.135.34.114");//用户IP地址
        jsonObject.put("notify_url",notify_url);//异步通知地址
        jsonObject.put("return_url",return_url);//同步通知地址
        jsonObject.put("timestamp",format);//支付类型

        String stringSignTemp = Asc.formatUrlMap(map, false, false)+key;
        String sign = MD5.getMd5(stringSignTemp,32).toUpperCase();
        jsonObject.put("sign",sign);//签名
        System.out.println(sign);
        System.out.println(jsonObject);
        String wxResult = HttpClientUtil.doPostJson(payUrl,jsonObject.toJSONString());
        System.out.println(wxResult);

        JSONObject jsonObject1 = JSONObject.parseObject(wxResult);

        System.out.println(jsonObject1);
        if(jsonObject1.get("code")!=null&&jsonObject1.get("code").toString().equals("0")){
            iFiveOrderService.insertByAddBo(bo) ;
        }else {
            return AjaxResult.error("请稍后再试!");
        }
        return AjaxResult.success(wxResult);
    }

    /**
     * 微信支付回调
     *
     * @param xmlData
     * @return
     * @throws WxPayException
     */
    @ApiOperation(value = "支付回调通知处理")
    @PostMapping("/notifyUrl")
    @Transactional
    public String parseOrderNotifyResult(@RequestBody String xmlData, HttpRequest request) throws WxPayException {
        logger.error("进入支付回调");
        try {
            System.out.println(xmlData);
            JSONObject jsonObject1 = JSONObject.parseObject(xmlData);
            if(jsonObject1.get("status").equals("2")){
                QueryWrapper queryWrapper=new QueryWrapper();
                queryWrapper.eq("number",jsonObject1.get("out_trade_no"));
                FiveOrder one = iFiveOrderService.getOne(queryWrapper);
                if(one!=null&&one.getCode()==1){
                    one.setCode(0);
                    iFiveOrderService.updateById(one);
                    //支付成功
                    FiveUser byId = iFiveUserService.getById(one.getUid());
                    //修改会员

                    Integer Cloudw=0;
                    Integer StockRight=0;
                    if(one!=null&&one.getGrade()==1){
                        byId.setIdentity(1);
                         Cloudw=100000;
                         StockRight=1000;
                    }
                    if(one!=null&&one.getGrade()==2){
                        byId.setIdentityx(1);
                        Cloudw=300000;
                        StockRight=3000;
                    }
                    if(one!=null&&one.getGrade()==3){
                        byId.setIdentityg(1);
                        Cloudw=600000;
                        StockRight=6000;
                    }
                    byId.setCloudw(byId.getCloudw()+Cloudw);
                    byId.setStockRight(byId.getStockRight()+StockRight);
                    iFiveRecordService.insertByAdd(byId.getId(),new BigDecimal(Cloudw),1,3);
                    iFiveRecordService.insertByAdd(byId.getId(),new BigDecimal(StockRight),6,3);
                    //修改会员 添加金额  添加记录
                    iFiveUserService.updateById(byId);
                    //添加上级金额  记录
                    if(byId.getInvitationId()!=null&&byId.getInvitationId()!=0){
                        FiveUser byId1 = iFiveUserService.getById(byId.getInvitationId());
                        iFiveRecordService.insertByAdd(byId1.getId(),new BigDecimal(599),3,2);
                        byId1.setBalance(byId1.getBalance().add(new BigDecimal(599)));
                        iFiveUserService.updateById(byId1);
                    }
                } }
        } catch (Exception e) {
            logger.error("支付回调失败：{}", e.getMessage(), e);
            return WxPayNotifyResponse.fail("失败");
        }
        return "SUCCESS";
    }

    /**
     * 微信支付回调
     *
     * @param
     * @return
     * @throws WxPayException
     */
    @ApiOperation(value = "手动添加会员")
    @PostMapping("/huiyuan")
    @Transactional
    public AjaxResult huiyuan(String  phone, Integer  type) throws WxPayException {

        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("phone",phone);
        FiveUser one1 = iFiveUserService.getOne(queryWrapper);

        if(one1!=null){
            Integer Cloudw=0;
            Integer StockRight=0;
            Integer Amount=0;
            if(type==1){
                one1.setIdentity(1);
                Cloudw=100000;
                StockRight=1000;
                Amount=200;
            }
            if(type==2){
                one1.setIdentityx(1);
                Cloudw=300000;
                StockRight=3000;
                Amount=500;
            }
            if(type==3){
                one1.setIdentityg(1);
                Cloudw=600000;
                StockRight=6000;
                Amount=1000;
            }

            FiveOrderAddBo bo =new FiveOrderAddBo();
            String orderId = String.valueOf(UUIDUtils.randomLong());
            bo.setUid(one1.getId());
            bo.setAmount(Amount);
            bo.setCode(0);
            bo.setNumber(orderId);
            bo.setGrade(type);
            iFiveOrderService.insertByAddBo(bo) ;
            one1.setCloudw(one1.getCloudw()+Cloudw);
            one1.setStockRight(one1.getStockRight()+StockRight);
            iFiveRecordService.insertByAdd(one1.getId(),new BigDecimal(Cloudw),1,3);
            iFiveRecordService.insertByAdd(one1.getId(),new BigDecimal(StockRight),6,3);
            //修改会员 添加金额  添加记录
            iFiveUserService.updateById(one1);
            //添加上级金额  记录
            if(one1.getInvitationId()!=null&&one1.getInvitationId()!=0){
                FiveUser byId1 = iFiveUserService.getById(one1.getInvitationId());
                iFiveRecordService.insertByAdd(byId1.getId(),new BigDecimal(599),3,2);
                byId1.setBalance(byId1.getBalance().add(new BigDecimal(599)));
                iFiveUserService.updateById(byId1);
            }
        }
        if(one1==null){
            return AjaxResult.error("会员不存在");
        }
        return AjaxResult.success();
    }

//    /**
//     * 新增order
//     */
//    @ApiOperation("支付")
//
//    @Log(title = "order", businessType = BusinessType.INSERT)
//    @PostMapping("/pay")
//    public AjaxResult<Void> pay(@RequestBody FiveOrderAddBo bo) throws ParseException {
//        Long userId = loginTokenService.getLoginUserId(ServletUtils.getRequest());
//        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd");
//        Date date = new Date(System.currentTimeMillis());
//        System.out.println(df1.format(date));
//
//        QueryWrapper queryWrapper=new QueryWrapper();
//        queryWrapper.eq("uid",userId);
//        queryWrapper.like("create_time",df1.format(date));
//        queryWrapper.orderByDesc("create_time");
//        List<FiveOrder> list = iFiveOrderService.list(queryWrapper);
//        if(list.size()>0) {
//            Date createTime = list.get(0).getCreateTime();
//            DateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
//            Date d1 = df.parse(df.format(createTime));
//
//            Date d2 = df.parse(df.format(date));
//
//            long diff = d2.getTime() - d1.getTime();
//
//            long time = diff / (1000 * 60);
//            if (time <= 5) {
//                throw new RuntimeException("支付频繁，请稍后五分钟重试");
//            }
//        }
//        if(userId==null){
//            userId=bo.getUid();
//        }
//
//        if(userId==null){
//            throw new RuntimeException("请重新支付");
//        }
//        String orderId = String.valueOf(UUIDUtils.randomLong());
//        String nonceStr = UUIDUtils.uuid2();
//        bo.setUid(userId);
//        bo.setAmount(400);
//        bo.setCode(1);
//        bo.setNumber(orderId);
//        Map map=new HashMap();
//
//        FiveBasics byId = iFiveBasicsService.getById(3);
//        String uuu=null;
//
//        String fxsign = fxsign("10106", nonceStr, "400", orderId,"http://bitfil.cc/#/home/assets","http://bitfil.cc/demo/demo/order/notifyUrl",byId.getConent());
//
//                if( byId.getTitle()==1){
//                    map.put("merId","10106");//商务号
//                    map.put("orderId",orderId);//商户订单号
//                    map.put("orderAmt","400");//支付金额
//                    map.put("channel",byId.getConent());//商户订单号
//                    map.put("desc","hu");//商品名称
//                    map.put("attch","hu");//商品名称
//                    map.put("ip","103.135.34.114");//用户IP地址
//                    map.put("notifyUrl","http://bitfil.cc/demo/demo/order/notifyUrl");//异步通知地址
//                    map.put("returnUrl","http://bitfil.cc/#/home/assets");//同步通知地址
//                    map.put("nonceStr",nonceStr);//支付类型
//                    map.put("sign",fxsign);//签名
//                }
//        System.out.println(uuu);
//        String wxResult = HttpClientUtil.doPost("http://api.dimila.com",map);
//        JSONObject jsonObject = JSONObject.parseObject(wxResult);
//
//        // 获取参数返回的access_token
//      //  String mp = jsonObject.get("access_token").toString();
//        System.out.println(jsonObject);
//        if(jsonObject.get("code").toString().equals("1")){
//            iFiveOrderService.insertByAddBo(bo) ;
//        }
//        return AjaxResult.success(wxResult);
//    }


//    public static  String  fxsign( String merId ,String nonceStr ,String orderAmt ,String orderId,String returnUrl ,String notifyUrl,String channel){
//        String ss =  "attch=hu&channel="+channel+"&desc=hu&ip=103.135.34.114"+
//                "&merId="+merId+"&nonceStr="+nonceStr+"&notifyUrl="+notifyUrl+
//                "&orderAmt="+orderAmt+"&orderId="+orderId+"&returnUrl="+returnUrl+"&key=QGkdxADZSHrRyBnUzbICgvOweupMEoVN";
//        System.out.println(ss);
//        String md5 = MD5.getMd5(ss,32).toUpperCase();
//        System.out.println(md5);
//        return md5;
//    }
//    /**
//     * 微信支付回调
//     *
//     * @param xmlData
//     * @return
//     * @throws WxPayException
//     */
//    @ApiOperation(value = "支付回调通知处理")
//    @PostMapping("/notifyUrl")
//    @Transactional
//    public String parseOrderNotifyResult(@RequestBody String xmlData, HttpRequest request) throws WxPayException {
//        logger.error("进入支付回调");
//        try {
//            System.out.println(xmlData);
//            String[] split = xmlData.split("&");
//            Map map=new HashMap();
//            for (String s : split) {
//                String[] split1 = s.split("=");
//                map.put(split1[0],split1[1]);
//            }
//            if(map.get("status").equals("0")){
//    QueryWrapper queryWrapper=new QueryWrapper();
//    queryWrapper.eq("number",map.get("orderId"));
//    FiveOrder one = iFiveOrderService.getOne(queryWrapper);
//    if(one!=null&&one.getCode()==1){
//        one.setCode(0);
//        iFiveOrderService.updateById(one);
//        //支付成功
//        FiveUser byId = iFiveUserService.getById(one.getUid());
//        //修改会员
//        byId.setIdentity(1);
//        byId.setBalance(byId.getBalance().add(new BigDecimal(399)));
//        byId.setCloudw(byId.getCloudw()+100000);
//        byId.setStockRight(byId.getStockRight()+100000);
//
//        //修改会员 添加金额  添加记录
//        iFiveRecordService.insertByAdd(byId.getId(),new BigDecimal(399),1,3);
//        iFiveRecordService.insertByAdd(byId.getId(),new BigDecimal(100000),2,3);
//        iFiveUserService.updateById(byId);
//        //添加上级金额  记录
//        if(byId.getInvitationId()!=null){
//            FiveUser byId1 = iFiveUserService.getById(byId.getInvitationId());
//            byId1.setBalance(byId.getBalance().add(new BigDecimal(399)));
//            byId1.setCloudw(byId.getCloudw()+10000);
//            byId1.setBalance(byId.getBalance().add(new BigDecimal(299)));
//            iFiveUserService.updateById(byId1);
//        }
//    } }
//        } catch (Exception e) {
//            logger.error("支付回调失败：{}", e.getMessage(), e);
//            return WxPayNotifyResponse.fail("失败");
//        }
//        return "success";
//    }






    /**
     * 查询order列表
     */
    @ApiOperation("查询order列表")
    @PreAuthorize("@ss.hasPermi('demo:order:list')")
    @GetMapping("/list")
    public TableDataInfo<FiveOrderVo> list(FiveOrderQueryBo bo) {
        startPage();
        List<FiveOrderVo> list = iFiveOrderService.queryList(bo);
        return getDataTable(list);
    }

    /**
     * 导出order列表
     */
    @ApiOperation("导出order列表")
    @PreAuthorize("@ss.hasPermi('demo:order:export')")
    @Log(title = "order", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult<FiveOrderVo> export(FiveOrderQueryBo bo) {
        List<FiveOrderVo> list = iFiveOrderService.queryList(bo);
        ExcelUtil<FiveOrderVo> util = new ExcelUtil<FiveOrderVo>(FiveOrderVo.class);
        return util.exportExcel(list, "order");
    }

//    /**
//     * 获取order详细信息
//     */
//    @ApiOperation("获取order详细信息")
//    @PreAuthorize("@ss.hasPermi('demo:order:query')")
//    @GetMapping("/{id}")
//    public AjaxResult<FiveOrderVo> getInfo(@PathVariable("id" ) Long id) {
//        return AjaxResult.success(iFiveOrderService.queryById(id));
//    }
//
//    /**
//     * 新增order
//     */
//    @ApiOperation("新增order")
//    @PreAuthorize("@ss.hasPermi('demo:order:add')")
//    @Log(title = "order", businessType = BusinessType.INSERT)
//    @PostMapping()
//    public AjaxResult<Void> add(@RequestBody FiveOrderAddBo bo) {
//        return toAjax(iFiveOrderService.insertByAddBo(bo) ? 1 : 0);
//    }
//
//    /**
//     * 修改order
//     */
//    @ApiOperation("修改order")
//    @PreAuthorize("@ss.hasPermi('demo:order:edit')")
//    @Log(title = "order", businessType = BusinessType.UPDATE)
//    @PutMapping()
//    public AjaxResult<Void> edit(@RequestBody FiveOrderEditBo bo) {
//        return toAjax(iFiveOrderService.updateByEditBo(bo) ? 1 : 0);
//    }
//
//    /**
//     * 删除order
//     */
//    @ApiOperation("删除order")
//    @PreAuthorize("@ss.hasPermi('demo:order:remove')")
//    @Log(title = "order" , businessType = BusinessType.DELETE)
//    @DeleteMapping("/{ids}")
//    public AjaxResult<Void> remove(@PathVariable Long[] ids) {
//        return toAjax(iFiveOrderService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
//    }
}
