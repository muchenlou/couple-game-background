package com.ruoyi.demo.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;
import java.util.Arrays;

import cn.hutool.core.bean.BeanUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.demo.domain.FiveAddress;
import com.ruoyi.demo.domain.FiveUser;
import com.ruoyi.demo.service.IFiveUserService;
import com.ruoyi.demo.service.LoginTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.demo.vo.FiveAddressVo;
import com.ruoyi.demo.bo.FiveAddressQueryBo;
import com.ruoyi.demo.bo.FiveAddressAddBo;
import com.ruoyi.demo.bo.FiveAddressEditBo;
import com.ruoyi.demo.service.IFiveAddressService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * addressController
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Api(value = "address控制器", tags = {"address管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/demo/address")
public class FiveAddressController extends BaseController {

    private final IFiveAddressService iFiveAddressService;
    private final LoginTokenService loginTokenService;
    @Autowired
    private final IFiveUserService iFiveUserService;
    /**
     * 获取address详细信息
     */
    @ApiOperation("获取address详细信息")
    @GetMapping("/getInfo")
    public AjaxResult<FiveAddress> getInfo() {
        Long userId = loginTokenService.getLoginUserId(ServletUtils.getRequest());

        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("uid",userId);
        FiveAddress one = iFiveAddressService.getOne(queryWrapper);
        return AjaxResult.success(one);
    }

    /**
     * 新增address
     */
    @ApiOperation("邮寄")

    @PostMapping("/add")
    public AjaxResult<Void> add(@RequestBody FiveAddressAddBo bo) {
        Long userId = loginTokenService.getLoginUserId(ServletUtils.getRequest());
        bo.setUid(userId);
        FiveUser fiveUser=new FiveUser();
        fiveUser.setId(userId);
        fiveUser.setStatus(2);
       iFiveUserService.updateById(fiveUser);

        return toAjax(iFiveAddressService.insertByAddBo(bo) ? 1 : 0);
    }
//    /**
//     * 查询address列表
//     */
//    @ApiOperation("查询address列表")
//    @PreAuthorize("@ss.hasPermi('demo:address:list')")
//    @GetMapping("/list")
//    public TableDataInfo<FiveAddressVo> list(FiveAddressQueryBo bo) {
//        startPage();
//        List<FiveAddressVo> list = iFiveAddressService.queryList(bo);
//        return getDataTable(list);
//    }
//
//    /**
//     * 导出address列表
//     */
//    @ApiOperation("导出address列表")
//    @PreAuthorize("@ss.hasPermi('demo:address:export')")
//    @Log(title = "address", businessType = BusinessType.EXPORT)
//    @GetMapping("/export")
//    public AjaxResult<FiveAddressVo> export(FiveAddressQueryBo bo) {
//        List<FiveAddressVo> list = iFiveAddressService.queryList(bo);
//        ExcelUtil<FiveAddressVo> util = new ExcelUtil<FiveAddressVo>(FiveAddressVo.class);
//        return util.exportExcel(list, "address");
//    }
//
//    /**
//     * 获取address详细信息
//     */
//    @ApiOperation("获取address详细信息")
//    @PreAuthorize("@ss.hasPermi('demo:address:query')")
//    @GetMapping("/{id}")
//    public AjaxResult<FiveAddressVo> getInfo(@PathVariable("id" ) Long id) {
//        return AjaxResult.success(iFiveAddressService.queryById(id));
//    }
//
//    /**
//     * 新增address
//     */
//    @ApiOperation("新增address")
//    @PreAuthorize("@ss.hasPermi('demo:address:add')")
//    @Log(title = "address", businessType = BusinessType.INSERT)
//    @PostMapping()
//    public AjaxResult<Void> add(@RequestBody FiveAddressAddBo bo) {
//        return toAjax(iFiveAddressService.insertByAddBo(bo) ? 1 : 0);
//    }
//
//    /**
//     * 修改address
//     */
//    @ApiOperation("修改address")
//    @PreAuthorize("@ss.hasPermi('demo:address:edit')")
//    @Log(title = "address", businessType = BusinessType.UPDATE)
//    @PutMapping()
//    public AjaxResult<Void> edit(@RequestBody FiveAddressEditBo bo) {
//        return toAjax(iFiveAddressService.updateByEditBo(bo) ? 1 : 0);
//    }
//
//    /**
//     * 删除address
//     */
//    @ApiOperation("删除address")
//    @PreAuthorize("@ss.hasPermi('demo:address:remove')")
//    @Log(title = "address" , businessType = BusinessType.DELETE)
//    @DeleteMapping("/{ids}")
//    public AjaxResult<Void> remove(@PathVariable Long[] ids) {
//        return toAjax(iFiveAddressService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
//    }
}
