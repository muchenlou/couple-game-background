package com.ruoyi.demo.controller;

import java.util.List;
import java.util.Arrays;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.demo.service.LoginTokenService;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.demo.vo.FiveRecordVo;
import com.ruoyi.demo.bo.FiveRecordQueryBo;
import com.ruoyi.demo.bo.FiveRecordAddBo;
import com.ruoyi.demo.bo.FiveRecordEditBo;
import com.ruoyi.demo.service.IFiveRecordService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * recordController
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Api(value = "record控制器", tags = {"record管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/demo/record")
public class FiveRecordController extends BaseController {

    private final IFiveRecordService iFiveRecordService;
    @Autowired
    private LoginTokenService loginTokenService;
    /**
     * 查询record列表
     */
    @ApiOperation("查询record列表")
    @PostMapping("/list")
    public AjaxResult<Page> list(@RequestBody FiveRecordQueryBo bo) {

        Long userId = loginTokenService.getLoginUserId(ServletUtils.getRequest());
        Page page =new Page(bo.getPageNum(), bo.getPageSize());
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("uid",userId);
        Page page1 = iFiveRecordService.page(page, queryWrapper);
        return AjaxResult.success(page1);
    }

//    /**
//     * 导出record列表
//     */
//    @ApiOperation("导出record列表")
//    @PreAuthorize("@ss.hasPermi('demo:record:export')")
//    @Log(title = "record", businessType = BusinessType.EXPORT)
//    @GetMapping("/export")
//    public AjaxResult<FiveRecordVo> export(FiveRecordQueryBo bo) {
//        List<FiveRecordVo> list = iFiveRecordService.queryList(bo);
//        ExcelUtil<FiveRecordVo> util = new ExcelUtil<FiveRecordVo>(FiveRecordVo.class);
//        return util.exportExcel(list, "record");
//    }

//    /**
//     * 获取record详细信息
//     */
//    @ApiOperation("获取record详细信息")
//    @PreAuthorize("@ss.hasPermi('demo:record:query')")
//    @GetMapping("/{id}")
//    public AjaxResult<FiveRecordVo> getInfo(@PathVariable("id" ) Long id) {
//        return AjaxResult.success(iFiveRecordService.queryById(id));
//    }

//    /**
//     * 新增record
//     */
//    @ApiOperation("新增record")
//    @PreAuthorize("@ss.hasPermi('demo:record:add')")
//    @Log(title = "record", businessType = BusinessType.INSERT)
//    @PostMapping()
//    public AjaxResult<Void> add(@RequestBody FiveRecordAddBo bo) {
//        return toAjax(iFiveRecordService.insertByAddBo(bo) ? 1 : 0);
//    }

//    /**
//     * 修改record
//     */
//    @ApiOperation("修改record")
//    @PreAuthorize("@ss.hasPermi('demo:record:edit')")
//    @Log(title = "record", businessType = BusinessType.UPDATE)
//    @PutMapping()
//    public AjaxResult<Void> edit(@RequestBody FiveRecordEditBo bo) {
//        return toAjax(iFiveRecordService.updateByEditBo(bo) ? 1 : 0);
//    }

//    /**
//     * 删除record
//     */
//    @ApiOperation("删除record")
//    @PreAuthorize("@ss.hasPermi('demo:record:remove')")
//    @Log(title = "record" , businessType = BusinessType.DELETE)
//    @DeleteMapping("/{ids}")
//    public AjaxResult<Void> remove(@PathVariable Long[] ids) {
//        return toAjax(iFiveRecordService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
//    }
}
