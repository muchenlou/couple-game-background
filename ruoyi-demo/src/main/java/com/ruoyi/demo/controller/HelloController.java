package com.ruoyi.demo.controller;

import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Xiaofei Mu
 * @version 1.0
 * @date 2022/8/13 11:35
 */
@RestController
@RequestMapping("/text")
public class HelloController {

    @RequestMapping("/hello")
    public String hello(){
        return "hello";
    }
}
