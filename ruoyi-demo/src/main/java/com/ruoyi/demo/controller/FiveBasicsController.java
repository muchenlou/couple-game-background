package com.ruoyi.demo.controller;

import java.util.List;
import java.util.Arrays;

import com.ruoyi.demo.domain.FiveBasics;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.demo.vo.FiveBasicsVo;
import com.ruoyi.demo.bo.FiveBasicsQueryBo;
import com.ruoyi.demo.bo.FiveBasicsAddBo;
import com.ruoyi.demo.bo.FiveBasicsEditBo;
import com.ruoyi.demo.service.IFiveBasicsService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * basicsController
 *
 * @author ruoyi
 * @date 2021-12-09
 */
@Api(value = "basics控制器", tags = {"basics管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/demo/basics")
public class FiveBasicsController extends BaseController {

    private final IFiveBasicsService iFiveBasicsService;
    /**
     * 修改basics
     */
    @ApiOperation("修改basics")
    @Log(title = "basics", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public AjaxResult<Void> update(@RequestBody FiveBasicsEditBo bo) {
        return toAjax(iFiveBasicsService.updateByEditBo(bo) ? 1 : 0);
    }

    /**
     * 查询basics列表
     */
    @ApiOperation("查询basics列表")
    @GetMapping("/all")
    public AjaxResult<List> all() {
        List<FiveBasics> list = iFiveBasicsService.list();
        return AjaxResult.success(list);
    }

    /**
     * 查询basics列表
     */
    @ApiOperation("查询basics列表")
    @PreAuthorize("@ss.hasPermi('demo:basics:list')")
    @GetMapping("/list")
    public TableDataInfo<FiveBasicsVo> list(FiveBasicsQueryBo bo) {
        startPage();
        List<FiveBasicsVo> list = iFiveBasicsService.queryList(bo);
        return getDataTable(list);
    }



    /**
     * 导出basics列表
     */
    @ApiOperation("导出basics列表")
    @PreAuthorize("@ss.hasPermi('demo:basics:export')")
    @Log(title = "basics", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult<FiveBasicsVo> export(FiveBasicsQueryBo bo) {
        List<FiveBasicsVo> list = iFiveBasicsService.queryList(bo);
        ExcelUtil<FiveBasicsVo> util = new ExcelUtil<FiveBasicsVo>(FiveBasicsVo.class);
        return util.exportExcel(list, "basics");
    }

    /**
     * 获取basics详细信息
     */
    @ApiOperation("获取basics详细信息")
    @PreAuthorize("@ss.hasPermi('demo:basics:query')")
    @GetMapping("/{id}")
    public AjaxResult<FiveBasicsVo> getInfo(@PathVariable("id" ) Long id) {
        return AjaxResult.success(iFiveBasicsService.queryById(id));
    }

    /**
     * 新增basics
     */
    @ApiOperation("新增basics")
    @PreAuthorize("@ss.hasPermi('demo:basics:add')")
    @Log(title = "basics", businessType = BusinessType.INSERT)
    @PostMapping()
    public AjaxResult<Void> add(@RequestBody FiveBasicsAddBo bo) {
        return toAjax(iFiveBasicsService.insertByAddBo(bo) ? 1 : 0);
    }

    /**
     * 修改basics
     */
    @ApiOperation("修改basics")
    @PreAuthorize("@ss.hasPermi('demo:basics:edit')")
    @Log(title = "basics", businessType = BusinessType.UPDATE)
    @PutMapping()
    public AjaxResult<Void> edit(@RequestBody FiveBasicsEditBo bo) {
        return toAjax(iFiveBasicsService.updateByEditBo(bo) ? 1 : 0);
    }

    /**
     * 删除basics
     */
    @ApiOperation("删除basics")
    @PreAuthorize("@ss.hasPermi('demo:basics:remove')")
    @Log(title = "basics" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult<Void> remove(@PathVariable Long[] ids) {
        return toAjax(iFiveBasicsService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }
}
