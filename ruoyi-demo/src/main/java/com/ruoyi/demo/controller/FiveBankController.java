package com.ruoyi.demo.controller;

import java.util.List;
import java.util.Arrays;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.demo.domain.FiveBank;
import com.ruoyi.demo.service.LoginTokenService;
import com.ruoyi.demo.util.MD5;
import lombok.RequiredArgsConstructor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.demo.vo.FiveBankVo;
import com.ruoyi.demo.bo.FiveBankQueryBo;
import com.ruoyi.demo.bo.FiveBankAddBo;
import com.ruoyi.demo.bo.FiveBankEditBo;
import com.ruoyi.demo.service.IFiveBankService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * bankController
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Api(value = "bank控制器", tags = {"bank管理"})
@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/demo/bank")
public class FiveBankController extends BaseController {

    private final IFiveBankService iFiveBankService;

    private final LoginTokenService loginTokenService;


    /**
     * 获取bank详细信息
     */
    @ApiOperation("实名认证")

    @GetMapping("/getInfo")
    public AjaxResult getInfo() {

        Long userId = loginTokenService.getLoginUserId(ServletUtils.getRequest());
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("uid",userId);
        FiveBank one = iFiveBankService.getOne(queryWrapper);

        if(one==null){
            return AjaxResult.error(111,"请实名认证！");
        }
        one.setTpwd("");
        return AjaxResult.success(one);
    }

    /**
          * 修改bank
          */
    @ApiOperation("修改bank")
    @Log(title = "bank", businessType = BusinessType.UPDATE)
    @PutMapping("/update")
    public AjaxResult<Void> edit(@RequestBody FiveBankEditBo bo) {
        bo.setTpwd(MD5.getMd5(bo.getTpwd(),32));

        return toAjax(iFiveBankService.updateByEditBo(bo) ? 1 : 0);
    }

    /**
     * 修改bank
     */
    @ApiOperation("判断密码")
    @Log(title = "bank", businessType = BusinessType.UPDATE)
    @PutMapping("/password")
    public AjaxResult password(String pwd) {
        Long userId = loginTokenService.getLoginUserId(ServletUtils.getRequest());
        String md5 = MD5.getMd5(pwd, 32);
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("uid",userId);
        queryWrapper.eq("tpwd",md5);
        FiveBank one = iFiveBankService.getOne(queryWrapper);
        if(one!=null){
            return AjaxResult.success("成功");
        }

        return AjaxResult.error("密码错误");
    }

    /**
          * 新增bank
          */
    @ApiOperation("新增bank")
    @Log(title = "bank", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    public AjaxResult<Void> addh5(@RequestBody FiveBankAddBo bo) {
        bo.setTpwd(MD5.getMd5(bo.getTpwd(),32));
        Long userId = loginTokenService.getLoginUserId(ServletUtils.getRequest());
        bo.setUid(userId);
        QueryWrapper queryWrapper =new QueryWrapper();
        queryWrapper.eq("uid",userId);
        FiveBank byId = iFiveBankService.getOne(queryWrapper);
        if(byId!=null){
            return AjaxResult.error("已认证");
        }
        return toAjax(iFiveBankService.insertByAddBo(bo) ? 1 : 0);
    }


//    /**
//     * 新增bank
//     */
//    @ApiOperation("新增bank")
//    @Log(title = "bank", businessType = BusinessType.INSERT)
//    @PostMapping("/add")
//    public AjaxResult<Void> addh5(@RequestBody FiveBankAddBo bo) {
//        Long userId = loginTokenService.getLoginUserId(ServletUtils.getRequest());
//        bo.setUid(userId);
//        return toAjax(iFiveBankService.insertByAddBo(bo) ? 1 : 0);
//    }
//
//
//    /**
//     * 获取bank详细信息
//     */
//    @ApiOperation("获取bank详细信息")
//    @GetMapping("/getone")
//    public AjaxResult<FiveBank> getInfoq() {
//        Long userId = loginTokenService.getLoginUserId(ServletUtils.getRequest());
//        QueryWrapper queryWrapper=new QueryWrapper();
//        queryWrapper.eq("uid",userId);
//        FiveBank one = iFiveBankService.getOne(queryWrapper);
//        return AjaxResult.success(one);
//    }
//
//    /**
//     * 查询bank列表
//     */
//    @ApiOperation("查询bank列表")
//    @PreAuthorize("@ss.hasPermi('demo:bank:list')")
//    @GetMapping("/list")
//    public TableDataInfo<FiveBankVo> list(FiveBankQueryBo bo) {
//        startPage();
//        List<FiveBankVo> list = iFiveBankService.queryList(bo);
//        return getDataTable(list);
//    }
//
//    /**
//     * 导出bank列表
//     */
//    @ApiOperation("导出bank列表")
//    @PreAuthorize("@ss.hasPermi('demo:bank:export')")
//    @Log(title = "bank", businessType = BusinessType.EXPORT)
//    @GetMapping("/export")
//    public AjaxResult<FiveBankVo> export(FiveBankQueryBo bo) {
//        List<FiveBankVo> list = iFiveBankService.queryList(bo);
//        ExcelUtil<FiveBankVo> util = new ExcelUtil<FiveBankVo>(FiveBankVo.class);
//        return util.exportExcel(list, "bank");
//    }
//
//    /**
//     * 获取bank详细信息
//     */
//    @ApiOperation("获取bank详细信息")
//    @PreAuthorize("@ss.hasPermi('demo:bank:query')")
//    @GetMapping("/{id}")
//    public AjaxResult<FiveBankVo> getInfo(@PathVariable("id" ) Long id) {
//        return AjaxResult.success(iFiveBankService.queryById(id));
//    }
//
//    /**
//     * 新增bank
//     */
//    @ApiOperation("新增bank")
//    @PreAuthorize("@ss.hasPermi('demo:bank:add')")
//    @Log(title = "bank", businessType = BusinessType.INSERT)
//    @PostMapping()
//    public AjaxResult<Void> add(@RequestBody FiveBankAddBo bo) {
//        return toAjax(iFiveBankService.insertByAddBo(bo) ? 1 : 0);
//    }
//
//    /**
//     * 修改bank
//     */
//    @ApiOperation("修改bank")
//    @PreAuthorize("@ss.hasPermi('demo:bank:edit')")
//    @Log(title = "bank", businessType = BusinessType.UPDATE)
//    @PutMapping()
//    public AjaxResult<Void> edit(@RequestBody FiveBankEditBo bo) {
//        return toAjax(iFiveBankService.updateByEditBo(bo) ? 1 : 0);
//    }
//
//    /**
//     * 删除bank
//     */
//    @ApiOperation("删除bank")
//    @PreAuthorize("@ss.hasPermi('demo:bank:remove')")
//    @Log(title = "bank" , businessType = BusinessType.DELETE)
//    @DeleteMapping("/{ids}")
//    public AjaxResult<Void> remove(@PathVariable Long[] ids) {
//        return toAjax(iFiveBankService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
//    }
}
