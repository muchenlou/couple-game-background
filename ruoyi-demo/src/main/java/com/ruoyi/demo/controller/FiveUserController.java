package com.ruoyi.demo.controller;

import java.math.BigDecimal;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

import cn.hutool.core.bean.BeanUtil;
import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.utils.ServletUtils;
import com.ruoyi.demo.bo.FiveRecordAddBo;
import com.ruoyi.demo.domain.FiveOrder;
import com.ruoyi.demo.domain.FiveUser;
import com.ruoyi.demo.service.IFiveOrderService;
import com.ruoyi.demo.service.IFiveRecordService;
import com.ruoyi.demo.service.LoginTokenService;
import com.ruoyi.demo.util.*;

import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.demo.vo.FiveUserVo;
import com.ruoyi.demo.bo.FiveUserQueryBo;
import com.ruoyi.demo.bo.FiveUserAddBo;
import com.ruoyi.demo.bo.FiveUserEditBo;
import com.ruoyi.demo.service.IFiveUserService;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

/**
 * userController
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Api(value = "user控制器", tags = {"user管理"})
//@RequiredArgsConstructor(onConstructor_ = @Autowired)
@RestController
@RequestMapping("/demo/user")
public class FiveUserController extends BaseController {


    @Autowired
    private IFiveUserService iFiveUserService;

    @Autowired
    private LoginTokenService loginTokenService;

    @Autowired
    private  IFiveRecordService iFiveRecordService;
    @Autowired
    private  IFiveOrderService iFiveOrderService;




    /**
     * 获取user详细信息
     */
    @ApiOperation("签到")
    @GetMapping("/SignIn")
    public AjaxResult SignIn(Long uid) {
        Long userId = loginTokenService.getLoginUserId(ServletUtils.getRequest());
        if(uid!=null){
            userId=uid;
        }
        FiveUserVo fiveUserVo = iFiveUserService.queryById(userId);
        if(fiveUserVo.getSignIn()==1){
            return AjaxResult.error("已签到");
        }
        Integer money=299;
        Integer  guquan=0;
        Integer StockRight=0;
        if(fiveUserVo!=null){
           if( fiveUserVo.getIdentity()==1){
               money=money+999;
               guquan=guquan+1000;

           }
            if( fiveUserVo.getIdentityx()==1){
                money=money+1999;
                guquan=guquan+3000;
            }
            if( fiveUserVo.getIdentityg()==1){
                money=money+3999;
                guquan=guquan+6000;
            }
        }
        iFiveRecordService.insertByAdd(userId,new BigDecimal(money),3,5);
//        if(guquan>0){
//            iFiveRecordService.insertByAdd(userId,new BigDecimal(guquan),6,5);
//        }

        FiveUser fiveUser=new FiveUser();
        fiveUser.setId(userId);
        fiveUser.setSignIn(1);
        fiveUser.setBalance(new BigDecimal(fiveUserVo.getBalance()+money));
       // fiveUser.setStockRight(fiveUserVo.getStockRight()+guquan);
        boolean b = iFiveUserService.updateById(fiveUser);
        if(b){
            return AjaxResult.success();
        }
        return AjaxResult.error("签到失败");
    }



    /**
     * 获取user详细信息
     */
    @ApiOperation("加100000")
    @GetMapping("/updatewxbyi")
    public AjaxResult updatewxbyi() {
        iFiveUserService.updatewxbyi();
        return AjaxResult.success();
    }
    /**
     * 获取user详细信息
     */
    @ApiOperation("加200000")
    @GetMapping("/updatewxber")
    public AjaxResult updatewxber() {
        iFiveUserService.updatewxber();
        return AjaxResult.success();

    }

    /**
     * 获取user详细信息
     */
    @ApiOperation("币种转换")
    @GetMapping("/transfer")
    public AjaxResult transfer(Integer num ,Integer type) throws Exception {
        Long userId = loginTokenService.getLoginUserId(ServletUtils.getRequest());
        if(userId==null){
            userId=25L;
        }
        if(userId==null){
            return AjaxResult.error("重新登录");
        }
        FiveUserVo fiveUserVo = iFiveUserService.queryById(userId);



        Integer multiple =3;
        if(fiveUserVo.getIdentity()==1){
            multiple=multiple+10;
        }
        if(fiveUserVo.getIdentityx()==1){
            multiple=multiple+30;
        }
        if(fiveUserVo.getIdentityg()==1){
            multiple=multiple+60;
        }
        int ft=0;
        //现金
        if(type==1){
            if(fiveUserVo.getBalance()>=num){
                 ft = fiveUserVo.getWxb() + num*multiple;
                fiveUserVo.setWxb(ft);
                int i1 = fiveUserVo.getBalance() - num;
                fiveUserVo.setBalance(i1);


            }else {
                return AjaxResult.error("现金不足");
            }
        }
        //注册
        if(type==2){
            if(fiveUserVo.getSignicon()>=num){
                ft =  fiveUserVo.getWxb()+ num*multiple;
                fiveUserVo.setWxb(ft);
                fiveUserVo.setSignicon(fiveUserVo.getSignicon()-num);

            }else {
                return AjaxResult.error("注册币不足");
            }
        }
        //分红
        if(type==3){
            if(fiveUserVo.getCloudh()>=num){
                ft =  fiveUserVo.getWxb()+ num*multiple;
                fiveUserVo.setWxb(ft);
                fiveUserVo.setCloudh(fiveUserVo.getCloudh()-num);

            }else {
                return AjaxResult.error("分红币不足");
            }
        }
        iFiveRecordService.insertByAdd(userId,new BigDecimal(-num),type+2,6);
        iFiveRecordService.insertByAdd(userId,new BigDecimal(num*multiple),2,6);
        FiveUserEditBo fiveUserEditBo = BeanUtil.toBean(fiveUserVo, FiveUserEditBo.class);
        iFiveUserService.updateByEditBo(fiveUserEditBo);
        return AjaxResult.success();
    }



    /**
     * 查询user列表
     */
    @ApiOperation("后台统计")
    @GetMapping("/statistics")
    public AjaxResult<Map> statistics() {
        Map map=new HashMap();

        int count = iFiveUserService.count();

        QueryWrapper wrapper=new QueryWrapper();
        wrapper.eq("code","0");
        wrapper.select("COALESCE(sum(amount),0.00) as amountAll");
        Map map1 = iFiveOrderService.getMap(wrapper);
        SimpleDateFormat formatter= new SimpleDateFormat("yyyy-MM-dd");
        Date date = new Date(System.currentTimeMillis());
        System.out.println(formatter.format(date));
        wrapper.like("create_time",formatter.format(date));
        Map map2 = iFiveOrderService.getMap(wrapper);
        map.put("a",count);
        map.put("b",map2.get("amountAll"));
        map.put("c",map1.get("amountAll"));
        return AjaxResult.success(map);
    }

    /**
     * 获取user详细信息
     */
    @ApiOperation("token获取user")
    @GetMapping("/byid")
    public AjaxResult byid() {
        Long userId = loginTokenService.getLoginUserId(ServletUtils.getRequest());
        FiveUser byId = iFiveUserService.getById(userId);
        if(byId==null){
            return AjaxResult.error(401,"令牌失效");
        }
        return AjaxResult.success(byId);
    }




    /**
     * 获取user详细信息
     */
    @ApiOperation("提现")
    @GetMapping("/Withdrawal")
    public AjaxResult Withdrawal(Integer amount) throws ParseException {
        Long userId = loginTokenService.getLoginUserId(ServletUtils.getRequest());
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("uid",userId);
        queryWrapper.eq("code",0);
        queryWrapper.orderByDesc("update_time");
        List<FiveOrder> list = iFiveOrderService.list(queryWrapper);
        FiveOrder fiveOrder=null;
        if(list.size()>0){
             fiveOrder = list.get(0);
        }else {
            return AjaxResult.error("请升级会员！");
        }
        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
        Date date = new Date(System.currentTimeMillis());
        String format = df1.format(date);
        String format1 = df1.format(fiveOrder.getUpdateTime());

        Date fromDate2 = df1.parse(format1);
        Date toDate2 = df1.parse(format);

        long from2 = fromDate2.getTime();
        long to2 = toDate2.getTime();
        int hours = (int) ((to2 - from2) / (1000 * 60 * 60));

       int day=hours/24;
        int  hour=hours-day*24;
        String time=day+"天"+hour+"小时";
        return AjaxResult.success(time);
    }

//    public static void main(String[] args) throws ParseException {
//
//        DateFormat df1 = new SimpleDateFormat("yyyy-MM-dd HH:mm");
//        Date date = new Date(System.currentTimeMillis());
//        String format = df1.format(date);
//        Date fromDate2 = df1.parse("2018-03-01 12:00");
//        Date toDate2 = df1.parse("2018-03-12 12:00");
//        long from2 = fromDate2.getTime();
//        long to2 = toDate2.getTime();
//        int hours = (int) ((to2 - from2) / (1000 * 60 * 60));
//        System.out.println("两个时间之间的小时差为：" + hours);
//
//
//
//
//    }



    /**
     * 新增user
     */

    @ApiOperation("login")
    @PostMapping("/login")
    public AjaxResult login(@RequestBody FiveUserAddBo bo) throws Exception {
        FiveUser one =null;
        //判断有没有用户
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("phone",bo.getPhone());
        one = iFiveUserService.getOne(queryWrapper);

        if(one==null){
            FiveUser one1=null;
            QueryWrapper queryWrapper1=new QueryWrapper();

            //邀请码
            if(bo.getCode()!=null){
                queryWrapper1.eq("code",bo.getCode());
                 one1 = iFiveUserService.getOne(queryWrapper1);
            }

            if (one1!=null){
                FiveRecordAddBo bo1=new FiveRecordAddBo();
                bo1.setEvent(2);
                bo1.setCoin(4);
                bo1.setUid(one1.getId());
                bo1.setAmount(new BigDecimal(30));
           //     iFiveRecordService.insertByAddBo(bo1);
             iFiveRecordService.insertByAdd(one1.getId(),new BigDecimal(200),4,2);
                one1.setSignicon(200);
                bo.setInvitationId(one1.getId());
                iFiveUserService.updateById(one1);
            }
            Boolean aBoolean = iFiveUserService.insertByAddBo(bo);
            if(aBoolean){
                one = iFiveUserService.getOne(queryWrapper);
            }
            iFiveRecordService.insertByAdd(one.getId(),new BigDecimal(199),3,1);
            iFiveRecordService.insertByAdd(one.getId(),new BigDecimal(5000),1,1);
        }
        //获取token
        ApiToken token = loginTokenService.creatToken(one.getId());
        AccessTokenResponse resp = AccessTokenResponse.builder()
                .accessToken(token.getAccessToken())
                .expiresIn(token.getAccessTokenExpireTime().getTime() / 1000 - System.currentTimeMillis() / 1000)
                .refreshToken(token.getRefreshToken())
                .user(one)
                .build();

        return  AjaxResult.success(resp);
    }



    /**
     //     * 获取user详细信息
     //     */
//    @ApiOperation("黄金邮寄")
//    @GetMapping("/huangjin")
//    public AjaxResult huangjin() {
//        Long userId = loginTokenService.getLoginUserId(ServletUtils.getRequest());
//        FiveUser fiveUser=new FiveUser();
//        fiveUser.setId(userId);
//        fiveUser.setStatus(2);
//        return AjaxResult.success(iFiveUserService.updateById(fiveUser));
//    }
//
//
//    /**
//     * 获取user详细信息
//     */
//    @ApiOperation("购买黄金")
//    @GetMapping("/buyGold")
//    public AjaxResult buyGold() {
//        Long userId = loginTokenService.getLoginUserId(ServletUtils.getRequest());
//
//        FiveUser fiveUser=new FiveUser();
//        fiveUser.setId(userId);
//        fiveUser.setCloudh(0);
//        boolean b = iFiveUserService.updateById(fiveUser);
//        return AjaxResult.success(b);
//    }


    /**
     * 查询user列表
     */
    @ApiOperation("查询user列表")
//    @PreAuthorize("@ss.hasPermi('demo:user:list')")
    @GetMapping("/list")
    public TableDataInfo<FiveUserVo> list(FiveUserQueryBo bo) {
        startPage();
        List<FiveUserVo> list = iFiveUserService.queryList(bo);
        return getDataTable(list);
    }

    /**
     * 导出user列表
     */
    @ApiOperation("导出user列表")
//    @PreAuthorize("@ss.hasPermi('demo:user:export')")
    @Log(title = "user", businessType = BusinessType.EXPORT)
    @GetMapping("/export")
    public AjaxResult<FiveUserVo> export(FiveUserQueryBo bo) {
        List<FiveUserVo> list = iFiveUserService.queryList(bo);
        ExcelUtil<FiveUserVo> util = new ExcelUtil<FiveUserVo>(FiveUserVo.class);
        return util.exportExcel(list, "user");
    }



    /**
     * 获取user详细信息
     */
    @ApiOperation("获取user详细信息")
//    @PreAuthorize("@ss.hasPermi('demo:user:query')")
    @GetMapping("/{id}")
    public AjaxResult<FiveUserVo> getInfo(@PathVariable("id" ) Long id) {
        return AjaxResult.success(iFiveUserService.queryById(id));
    }

    /**
     * 新增user
     */
    @ApiOperation("新增user")
//    @PreAuthorize("@ss.hasPermi('demo:user:add')")
    @Log(title = "user", businessType = BusinessType.INSERT)
    @PostMapping()
    public AjaxResult<Void> add(@RequestBody FiveUserAddBo bo) throws Exception {
        return toAjax(iFiveUserService.insertByAddBo(bo) ? 1 : 0);
    }

    /**
     * 修改user
     */
    @ApiOperation("修改user")
//    @PreAuthorize("@ss.hasPermi('demo:user:edit')")
    @Log(title = "user", businessType = BusinessType.UPDATE)
    @PutMapping()
    public AjaxResult<Void> edit(@RequestBody FiveUserEditBo bo) {
        return toAjax(iFiveUserService.updateByEditBo(bo) ? 1 : 0);
    }

    /**
     * 删除user
     */
    @ApiOperation("删除user")
//    @PreAuthorize("@ss.hasPermi('demo:user:remove')")
    @Log(title = "user" , businessType = BusinessType.DELETE)
    @DeleteMapping("/{ids}")
    public AjaxResult<Void> remove(@PathVariable Long[] ids) {
        return toAjax(iFiveUserService.deleteWithValidByIds(Arrays.asList(ids), true) ? 1 : 0);
    }
}
