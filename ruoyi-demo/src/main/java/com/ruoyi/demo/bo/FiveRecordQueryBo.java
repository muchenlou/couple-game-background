package com.ruoyi.demo.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.Map;
import java.util.HashMap;

import java.math.BigDecimal;
import com.ruoyi.common.core.domain.BaseEntity;

/**
 * record分页查询对象 five_record
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("record分页查询对象")
public class FiveRecordQueryBo extends BaseEntity {

	/** 分页大小 */
	@ApiModelProperty("分页大小")
	private Integer pageSize;
	/** 当前页数 */
	@ApiModelProperty("当前页数")
	private Integer pageNum;
	/** 排序列 */
	@ApiModelProperty("排序列")
	private String orderByColumn;
	/** 排序的方向desc或者asc */
	@ApiModelProperty(value = "排序的方向", example = "asc,desc")
	private String isAsc;


	/** 用户id */
	@ApiModelProperty("用户id")
	private Long uid;
	/** 金额 */
	@ApiModelProperty("金额")
	private BigDecimal amount;
	/** 1余额2五连3wxb4黄金 */
	@ApiModelProperty("1余额2五连3wxb4黄金")
	private Integer  coin;
	/**  1注册  2邀请 */
	@ApiModelProperty(" 1注册  2邀请")
	private Integer  event;

}
