package com.ruoyi.demo.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;



/**
 * bank添加对象 five_bank
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Data
@ApiModel("bank添加对象")
public class FiveBankAddBo {

    /** $column.columnComment */
    @ApiModelProperty("姓名")
    private String name;
    @ApiModelProperty("用户id")
    private Long uid;
    /** $column.columnComment */
    @ApiModelProperty("身份证")
    private String identity;
    /** $column.columnComment */
    @ApiModelProperty("卡号")
    private String bankCard;
    /** $column.columnComment */
    @ApiModelProperty("开户行")
    private String bank;
    /** $column.columnComment */
    @ApiModelProperty("修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    /** $column.columnComment */
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    /** $column.columnComment */
    @ApiModelProperty("状态")
    private String status;

    private String  tpwd;
}
