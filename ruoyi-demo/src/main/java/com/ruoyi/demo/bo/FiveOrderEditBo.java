package com.ruoyi.demo.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;


/**
 * order编辑对象 five_order
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Data
@ApiModel("order编辑对象")
public class FiveOrderEditBo {


    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private Long id;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private Long uid;

    /** 单号 */
    @ApiModelProperty("单号")
    private String number;

    /** 支付状态 */
    @ApiModelProperty("支付状态")
    private String code;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private Long amount;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}
