package com.ruoyi.demo.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.Map;
import java.util.HashMap;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * 用户分页查询对象 five_user
 *
 * @author ruoyi
 * @date 2021-12-09
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("用户分页查询对象")
public class FiveUserQueryBo extends BaseEntity {

	/** 分页大小 */
	@ApiModelProperty("分页大小")
	private Integer pageSize;
	/** 当前页数 */
	@ApiModelProperty("当前页数")
	private Integer pageNum;
	/** 排序列 */
	@ApiModelProperty("排序列")
	private String orderByColumn;
	/** 排序的方向desc或者asc */
	@ApiModelProperty(value = "排序的方向", example = "asc,desc")
	private String isAsc;


	/** 手机号 */
	@ApiModelProperty("手机号")
	private String phone;
	/** 0普通会员  1.会员 */
	@ApiModelProperty("0普通会员  1.会员")
	private Long identity;
	/** 余额 */
	@ApiModelProperty("余额")
	private Long balance;
	/** wxb */
	@ApiModelProperty("wxb")
	private Long wxb;
	/**  云数五联币 */
	@ApiModelProperty(" 云数五联币")
	private Long cloudw;
	/** 云数黄金 */
	@ApiModelProperty("云数黄金")
	private Long cloudh;
	/** 邀请码 */
	@ApiModelProperty("邀请码")
	private String code;
	/** 股权 */
	@ApiModelProperty("股权")
	private Long stockRight;
	/** 邀请人id */
	@ApiModelProperty("邀请人id")
	private Long invitationId;
	/** 状态 */
	@ApiModelProperty("状态")
	private Long status;
	/** 二维码 */
	@ApiModelProperty("二维码")
	private String img;

}
