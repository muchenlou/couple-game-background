package com.ruoyi.demo.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;


/**
 * bank编辑对象 five_bank
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Data
@ApiModel("bank编辑对象")
public class FiveBankEditBo {


    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private Long id;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private String name;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private Long uid;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private String identity;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private String bankCard;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private String bank;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private String status;
    private String  tpwd;
}
