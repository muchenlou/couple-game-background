package com.ruoyi.demo.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;


import java.math.BigDecimal;

/**
 * record添加对象 five_record
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Data
@ApiModel("record添加对象")
public class FiveRecordAddBo {

    /** 用户id */
    @ApiModelProperty("用户id")
    private Long uid;
    /** 金额 */
    @ApiModelProperty("金额")
    private BigDecimal amount;
    /** 1余额2五连3wxb4黄金 */
    @ApiModelProperty("1余额2五连3wxb4黄金")
    private Integer  coin;
    /**  1注册  2邀请 */
    @ApiModelProperty(" 1注册  2邀请")
    private Integer  event;
    /** 创建时间 */
    @ApiModelProperty("创建时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
}
