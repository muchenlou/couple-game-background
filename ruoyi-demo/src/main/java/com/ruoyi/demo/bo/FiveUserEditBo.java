package com.ruoyi.demo.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;


/**
 * 用户编辑对象 five_user
 *
 * @author ruoyi
 * @date 2021-12-09
 */
@Data
@ApiModel("用户编辑对象")
public class FiveUserEditBo extends FiveUserAddBo {


    /** id */
    @ApiModelProperty("id")
    private Long id;

    /** 手机号 */
    @ApiModelProperty("手机号")
    private String phone;

    /** 0普通会员  1.会员 */
    @ApiModelProperty("0普通会员  1.会员")
    private Long identity;

    /** 余额 */
    @ApiModelProperty("余额")
    private Long balance;

    /** wxb */
    @ApiModelProperty("wxb")
    private Long wxb;

    /**  云数五联币 */
    @ApiModelProperty(" 云数五联币")
    private Long cloudw;

    /** 云数黄金 */
    @ApiModelProperty("云数黄金")
    private Long cloudh;

    /** 邀请码 */
    @ApiModelProperty("邀请码")
    private String code;

    /** 股权 */
    @ApiModelProperty("股权")
    private Long stockRight;

    /** 邀请人id */
    @ApiModelProperty("邀请人id")
    private Long invitationId;

    /** 状态 */
    @ApiModelProperty("状态")
    private Long status;

    /** 修改时间 */
    @ApiModelProperty("修改时间")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;

    /** 二维码 */
    @ApiModelProperty("二维码")
    private String img;

    private Integer signIn;

    private  Integer  signicon;

    private String	locations;

}
