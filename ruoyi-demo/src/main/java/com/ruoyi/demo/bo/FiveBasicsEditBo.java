package com.ruoyi.demo.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;


/**
 * basics编辑对象 five_basics
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Data
@ApiModel("basics编辑对象")
public class FiveBasicsEditBo {


    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private Long id;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private String conent;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private String  title;
}
