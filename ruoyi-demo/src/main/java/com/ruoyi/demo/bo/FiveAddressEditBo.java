package com.ruoyi.demo.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;


/**
 * address编辑对象 five_address
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Data
@ApiModel("address编辑对象")
public class FiveAddressEditBo {


    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private Long id;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private Long uid;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private String address;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private String phone;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private String name;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private Long status;

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
}
