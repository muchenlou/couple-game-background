package com.ruoyi.demo.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;



/**
 * address添加对象 five_address
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Data
@ApiModel("address添加对象")
public class FiveAddressAddBo {

    /** $column.columnComment */
    @ApiModelProperty("用户id")
    private Long uid;
    /** $column.columnComment */
    @ApiModelProperty("地址")
    private String address;
    /** $column.columnComment */
    @ApiModelProperty("手机号")
    private String phone;
    /** $column.columnComment */
    @ApiModelProperty("姓名")
    private String name;

}
