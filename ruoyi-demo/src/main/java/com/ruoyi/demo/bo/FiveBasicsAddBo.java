package com.ruoyi.demo.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;



/**
 * basics添加对象 five_basics
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Data
@ApiModel("basics添加对象")
public class FiveBasicsAddBo {

    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private String conent;
    /** $column.columnComment */
    @ApiModelProperty("$column.columnComment")
    private String  title;
}
