package com.ruoyi.demo.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.EqualsAndHashCode;

import java.util.Date;
import java.util.Map;
import java.util.HashMap;

import com.ruoyi.common.core.domain.BaseEntity;

/**
 * basics分页查询对象 five_basics
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Data
@EqualsAndHashCode(callSuper = true)
@ApiModel("basics分页查询对象")
public class FiveBasicsQueryBo extends BaseEntity {

	/** 分页大小 */
	@ApiModelProperty("分页大小")
	private Integer pageSize;
	/** 当前页数 */
	@ApiModelProperty("当前页数")
	private Integer pageNum;
	/** 排序列 */
	@ApiModelProperty("排序列")
	private String orderByColumn;
	/** 排序的方向desc或者asc */
	@ApiModelProperty(value = "排序的方向", example = "asc,desc")
	private String isAsc;


	/** $column.columnComment */
	@ApiModelProperty("$column.columnComment")
	private String conent;
	/** $column.columnComment */
	@ApiModelProperty("$column.columnComment")
	private String  title;

}
