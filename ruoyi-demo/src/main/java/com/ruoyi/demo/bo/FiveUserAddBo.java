package com.ruoyi.demo.bo;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import java.util.Date;



/**
 * 用户添加对象 five_user
 *
 * @author ruoyi
 * @date 2021-12-09
 */
@Data
@ApiModel("用户添加对象")
public class FiveUserAddBo {

    /** 手机号 */
    @ApiModelProperty("手机号")
    private String phone;

    /** 邀请码 */
    @ApiModelProperty("邀请码")
    private String code;
    /** 股权 */

    @ApiModelProperty("邀请人id")
    private Long invitationId;

}
