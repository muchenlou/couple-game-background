package com.ruoyi.demo.util;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

@Slf4j
@Data
@Builder
public class RefreshTokenResponse implements Serializable {
    private static final long serialVersionUID = 1L;

    @ApiModelProperty("accessToken")
    private String accessToken;
    @ApiModelProperty("refreshToken")
    private String refreshToken;
    @ApiModelProperty("expiresIn")
    private Long expiresIn;
}
