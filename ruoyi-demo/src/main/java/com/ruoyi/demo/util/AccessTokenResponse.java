package com.ruoyi.demo.util;

import com.ruoyi.demo.domain.FiveUser;

import io.swagger.annotations.ApiModelProperty;
import lombok.Builder;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.Serializable;

@Slf4j
@Data
@Builder
public class AccessTokenResponse implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("accessToken")
    private String accessToken;
    @ApiModelProperty("refreshToken")
    private String refreshToken;
    @ApiModelProperty("expiresIn")
    private Long expiresIn;
    @ApiModelProperty("用户信息")
    private FiveUser user;

}
