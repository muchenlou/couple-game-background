package com.ruoyi.demo.util;


import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

import java.io.Serializable;
import java.util.Date;


@Data
public class ApiToken implements Serializable {

    private static final long serialVersionUID = 1L;

    @ApiModelProperty("用户ID")
    private Long userId;
    @ApiModelProperty("accessToken")
    private String accessToken;
    @ApiModelProperty("refreshToken")
    private String refreshToken;
    @ApiModelProperty("accessTokenExpireTime")
    private Date accessTokenExpireTime;
    @ApiModelProperty("refreshTokenExpireTime")
    private Date refreshTokenExpireTime;
    @ApiModelProperty("updateTime")
    private Date updateTime;

}
