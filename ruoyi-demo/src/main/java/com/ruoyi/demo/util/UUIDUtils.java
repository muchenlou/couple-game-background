package com.ruoyi.demo.util;

import java.security.SecureRandom;
import java.util.UUID;

public class UUIDUtils {

    private static SecureRandom random = new SecureRandom();

    public UUIDUtils() {
    }

    public static String uuid() {
        return UUID.randomUUID().toString();
    }

    public static String uuid2() {
        return UUID.randomUUID().toString().replaceAll("-", "");
    }

    public static long randomLong() {
        return Math.abs(random.nextLong());
    }
}
