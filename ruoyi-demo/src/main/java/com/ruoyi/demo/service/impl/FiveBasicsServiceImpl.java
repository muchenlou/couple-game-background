package com.ruoyi.demo.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.pagehelper.Page;
import com.ruoyi.demo.bo.FiveBasicsAddBo;
import com.ruoyi.demo.bo.FiveBasicsQueryBo;
import com.ruoyi.demo.bo.FiveBasicsEditBo;
import com.ruoyi.demo.domain.FiveBasics;
import com.ruoyi.demo.mapper.FiveBasicsMapper;
import com.ruoyi.demo.vo.FiveBasicsVo;
import com.ruoyi.demo.service.IFiveBasicsService;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * basicsService业务层处理
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Service
public class FiveBasicsServiceImpl extends ServiceImpl<FiveBasicsMapper, FiveBasics> implements IFiveBasicsService {

    @Override
    public FiveBasicsVo queryById(Long id){
        FiveBasics db = this.baseMapper.selectById(id);
        return BeanUtil.toBean(db, FiveBasicsVo.class);
    }

    @Override
    public List<FiveBasicsVo> queryList(FiveBasicsQueryBo bo) {
        LambdaQueryWrapper<FiveBasics> lqw = Wrappers.lambdaQuery();
        lqw.eq(StrUtil.isNotBlank(bo.getConent()), FiveBasics::getConent, bo.getConent());
        lqw.eq(StrUtil.isNotBlank(bo.getTitle()), FiveBasics::getTitle, bo.getTitle());
        return entity2Vo(this.list(lqw));
    }

    /**
    * 实体类转化成视图对象
    *
    * @param collection 实体类集合
    * @return
    */
    private List<FiveBasicsVo> entity2Vo(Collection<FiveBasics> collection) {
        List<FiveBasicsVo> voList = collection.stream()
                .map(any -> BeanUtil.toBean(any, FiveBasicsVo.class))
                .collect(Collectors.toList());
        if (collection instanceof Page) {
            Page<FiveBasics> page = (Page<FiveBasics>)collection;
            Page<FiveBasicsVo> pageVo = new Page<>();
            BeanUtil.copyProperties(page,pageVo);
            pageVo.addAll(voList);
            voList = pageVo;
        }
        return voList;
    }

    @Override
    public Boolean insertByAddBo(FiveBasicsAddBo bo) {
        FiveBasics add = BeanUtil.toBean(bo, FiveBasics.class);
        validEntityBeforeSave(add);
        return this.save(add);
    }

    @Override
    public Boolean updateByEditBo(FiveBasicsEditBo bo) {
        FiveBasics update = BeanUtil.toBean(bo, FiveBasics.class);
        validEntityBeforeSave(update);
        return this.updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(FiveBasics entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return this.removeByIds(ids);
    }
}
