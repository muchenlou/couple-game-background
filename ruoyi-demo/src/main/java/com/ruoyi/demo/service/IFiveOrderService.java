package com.ruoyi.demo.service;

import com.ruoyi.demo.domain.FiveOrder;
import com.ruoyi.demo.vo.FiveOrderVo;
import com.ruoyi.demo.bo.FiveOrderQueryBo;
import com.ruoyi.demo.bo.FiveOrderAddBo;
import com.ruoyi.demo.bo.FiveOrderEditBo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Collection;
import java.util.List;

/**
 * orderService接口
 *
 * @author ruoyi
 * @date 2021-12-08
 */
public interface IFiveOrderService extends IService<FiveOrder> {
	/**
	 * 查询单个
	 * @return
	 */
	FiveOrderVo queryById(Long id);

	/**
	 * 查询列表
	 */
	List<FiveOrderVo> queryList(FiveOrderQueryBo bo);

	/**
	 * 根据新增业务对象插入order
	 * @param bo order新增业务对象
	 * @return
	 */
	Boolean insertByAddBo(FiveOrderAddBo bo);

	/**
	 * 根据编辑业务对象修改order
	 * @param bo order编辑业务对象
	 * @return
	 */
	Boolean updateByEditBo(FiveOrderEditBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
