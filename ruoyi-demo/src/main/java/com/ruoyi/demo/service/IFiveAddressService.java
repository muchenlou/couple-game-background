package com.ruoyi.demo.service;

import com.ruoyi.demo.domain.FiveAddress;
import com.ruoyi.demo.vo.FiveAddressVo;
import com.ruoyi.demo.bo.FiveAddressQueryBo;
import com.ruoyi.demo.bo.FiveAddressAddBo;
import com.ruoyi.demo.bo.FiveAddressEditBo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Collection;
import java.util.List;

/**
 * addressService接口
 *
 * @author ruoyi
 * @date 2021-12-08
 */
public interface IFiveAddressService extends IService<FiveAddress> {
	/**
	 * 查询单个
	 * @return
	 */
	FiveAddressVo queryById(Long id);

	/**
	 * 查询列表
	 */
	List<FiveAddressVo> queryList(FiveAddressQueryBo bo);

	/**
	 * 根据新增业务对象插入address
	 * @param bo address新增业务对象
	 * @return
	 */
	Boolean insertByAddBo(FiveAddressAddBo bo);

	/**
	 * 根据编辑业务对象修改address
	 * @param bo address编辑业务对象
	 * @return
	 */
	Boolean updateByEditBo(FiveAddressEditBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
