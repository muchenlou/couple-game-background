package com.ruoyi.demo.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.ruoyi.common.utils.file.FileUtils;
import com.ruoyi.demo.domain.FiveBasics;
import com.ruoyi.demo.mapper.FiveBasicsMapper;
import com.ruoyi.demo.service.IFiveBasicsService;
import com.ruoyi.demo.util.MD5;
import com.ruoyi.demo.util.PicUtils;
import com.ruoyi.demo.util.QRCodeMax;
import com.ruoyi.demo.util.QRCodeUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.pagehelper.Page;
import com.ruoyi.demo.bo.FiveUserAddBo;
import com.ruoyi.demo.bo.FiveUserQueryBo;
import com.ruoyi.demo.bo.FiveUserEditBo;
import com.ruoyi.demo.domain.FiveUser;
import com.ruoyi.demo.mapper.FiveUserMapper;
import com.ruoyi.demo.vo.FiveUserVo;
import com.ruoyi.demo.service.IFiveUserService;

import java.io.File;
import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * userService业务层处理
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Service
public class FiveUserServiceImpl extends ServiceImpl<FiveUserMapper, FiveUser> implements IFiveUserService {
    @Autowired
    private  IFiveBasicsService iFiveBasicsService;


    @Override
    public FiveUserVo queryById(Long id){
        FiveUser db = this.baseMapper.selectById(id);
        return BeanUtil.toBean(db, FiveUserVo.class);
    }

    @Override
    public List<FiveUserVo> queryList(FiveUserQueryBo bo) {
        LambdaQueryWrapper<FiveUser> lqw = Wrappers.lambdaQuery();
        lqw.eq(StrUtil.isNotBlank(bo.getPhone()), FiveUser::getPhone, bo.getPhone());
        lqw.eq(bo.getIdentity() != null, FiveUser::getIdentity, bo.getIdentity());
        lqw.eq(bo.getBalance() != null, FiveUser::getBalance, bo.getBalance());
        lqw.eq(bo.getWxb() != null, FiveUser::getWxb, bo.getWxb());
        lqw.eq(bo.getCloudw() != null, FiveUser::getCloudw, bo.getCloudw());
        lqw.eq(bo.getCloudh() != null, FiveUser::getCloudh, bo.getCloudh());
        lqw.eq(StrUtil.isNotBlank(bo.getCode()), FiveUser::getCode, bo.getCode());
        lqw.eq(bo.getStockRight() != null, FiveUser::getStockRight, bo.getStockRight());
        lqw.eq(bo.getInvitationId() != null, FiveUser::getInvitationId, bo.getInvitationId());
        lqw.eq(bo.getStatus() != null, FiveUser::getStatus, bo.getStatus());
        return entity2Vo(this.list(lqw));
    }

    /**
    * 实体类转化成视图对象
    *
    * @param collection 实体类集合
    * @return
    */
    private List<FiveUserVo> entity2Vo(Collection<FiveUser> collection) {
        List<FiveUserVo> voList = collection.stream()
                .map(any -> BeanUtil.toBean(any, FiveUserVo.class))
                .collect(Collectors.toList());
        if (collection instanceof Page) {
            Page<FiveUser> page = (Page<FiveUser>)collection;
            Page<FiveUserVo> pageVo = new Page<>();
            BeanUtil.copyProperties(page,pageVo);
            pageVo.addAll(voList);
            voList = pageVo;
        }
        return voList;
    }

    @Override
    public Boolean insertByAddBo(FiveUserAddBo bo) throws Exception {
        FiveUser add = BeanUtil.toBean(bo, FiveUser.class);
        BigDecimal bigDecimal = new BigDecimal(199);
        add.setBalance(bigDecimal);
        add.setCloudw(5000);
        String s = getlinkNo();
        String s1 = MD5.getMd5(s, 32).toUpperCase();
        QueryWrapper queryWrapper=new QueryWrapper();
        queryWrapper.eq("code",s);
        FiveUser fiveUser = this.baseMapper.selectOne(queryWrapper);
        if (fiveUser!=null){
            s = getlinkNo();
        }

        FiveBasics byId4 = iFiveBasicsService.getById(4);

       String  url =byId4.getConent()+"/login?key="+s;
     //   String destPath = "/ui/images";
   //     String encode = QRCodeUtils.encode(url, null, destPath, true);
      //  File bgImgFile=new File("D:/1/di.jpg");//背景图片
      //  File QrCodeFile = new File("D:/1/"+s+".png");//生成图片位置

        File bgImgFile=new File("/ui/img/di.jpg");//背景图片
        File QrCodeFile = new File("/ui/img/"+s+".png");//生成图片位置

        String note = "" ;//文字描述\
        String tui = "" ;//文字描述
        QRCodeMax. CreatQRCode(QrCodeFile,bgImgFile, 430, 430, url, note,tui, 0, 410, 1010, 0, 0, 0, 0);


        byte[] bytes = FileUtils.readFileToByteArray(QrCodeFile);
        long l = System.currentTimeMillis();
        bytes = PicUtils.compressPicForScale(bytes, 1000, "x");// 图片小于300kb
        System.out.println(System.currentTimeMillis() - l);
        FileUtils.writeByteArrayToFile(QrCodeFile, bytes);
        add.setImg(byId4.getConent()+"/"+s+".png");
        add.setCode(s);
        add.setLocations(s1);
        validEntityBeforeSave(add);
        boolean save = this.save(add);
        return save;
    }

    public static void main(String[] args) {

        System.out.println(  getlinkNo());
    }
    public  static  String getlinkNo() {
        String linkNo = "";
        // 用字符数组的方式随机
        String model = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
        char[] m = model.toCharArray();
        for (int j = 0; j < 6; j++) {
            char c = m[(int) (Math.random() * 36)];
            // 保证六位随机数之间没有重复的
            if (linkNo.contains(String.valueOf(c))) {
                j--;
                continue;
            }
            linkNo = linkNo + c;
        }
        return linkNo;
    }

    @Override
    public Boolean updateByEditBo(FiveUserEditBo bo) {
        FiveUser update = BeanUtil.toBean(bo, FiveUser.class);
        validEntityBeforeSave(update);
        return this.updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(FiveUser entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return this.removeByIds(ids);
    }

    @Override
    public void updatewxbyi() {
        this.baseMapper.updatewxbyi();
    }

    @Override
    public void updatewxber() {
        this.baseMapper.updatewxber();
    }
}
