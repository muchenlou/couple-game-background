package com.ruoyi.demo.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.pagehelper.Page;
import com.ruoyi.demo.bo.FiveAddressAddBo;
import com.ruoyi.demo.bo.FiveAddressQueryBo;
import com.ruoyi.demo.bo.FiveAddressEditBo;
import com.ruoyi.demo.domain.FiveAddress;
import com.ruoyi.demo.mapper.FiveAddressMapper;
import com.ruoyi.demo.vo.FiveAddressVo;
import com.ruoyi.demo.service.IFiveAddressService;

import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

/**
 * addressService业务层处理
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Service
public class FiveAddressServiceImpl extends ServiceImpl<FiveAddressMapper, FiveAddress> implements IFiveAddressService {

    @Override
    public FiveAddressVo queryById(Long id){
        FiveAddress db = this.baseMapper.selectById(id);
        return BeanUtil.toBean(db, FiveAddressVo.class);
    }

    @Override
    public List<FiveAddressVo> queryList(FiveAddressQueryBo bo) {
        LambdaQueryWrapper<FiveAddress> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getUid() != null, FiveAddress::getUid, bo.getUid());
        lqw.eq(StrUtil.isNotBlank(bo.getAddress()), FiveAddress::getAddress, bo.getAddress());
        lqw.eq(StrUtil.isNotBlank(bo.getPhone()), FiveAddress::getPhone, bo.getPhone());
        lqw.like(StrUtil.isNotBlank(bo.getName()), FiveAddress::getName, bo.getName());
        lqw.eq(bo.getStatus() != null, FiveAddress::getStatus, bo.getStatus());
        return entity2Vo(this.list(lqw));
    }

    /**
    * 实体类转化成视图对象
    *
    * @param collection 实体类集合
    * @return
    */
    private List<FiveAddressVo> entity2Vo(Collection<FiveAddress> collection) {
        List<FiveAddressVo> voList = collection.stream()
                .map(any -> BeanUtil.toBean(any, FiveAddressVo.class))
                .collect(Collectors.toList());
        if (collection instanceof Page) {
            Page<FiveAddress> page = (Page<FiveAddress>)collection;
            Page<FiveAddressVo> pageVo = new Page<>();
            BeanUtil.copyProperties(page,pageVo);
            pageVo.addAll(voList);
            voList = pageVo;
        }
        return voList;
    }

    @Override
    public Boolean insertByAddBo(FiveAddressAddBo bo) {
        FiveAddress add = BeanUtil.toBean(bo, FiveAddress.class);
        SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");//设置日期格式
        System.out.println(df.format(new Date()));// new Date()为获取当前系统时间
        String format = df.format(new Date());
        add.setCreateTime(format);
        validEntityBeforeSave(add);
        return this.save(add);
    }

    @Override
    public Boolean updateByEditBo(FiveAddressEditBo bo) {
        FiveAddress update = BeanUtil.toBean(bo, FiveAddress.class);
        validEntityBeforeSave(update);
        return this.updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(FiveAddress entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return this.removeByIds(ids);
    }
}
