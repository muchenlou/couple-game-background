package com.ruoyi.demo.service;

import com.ruoyi.demo.domain.FiveRecord;
import com.ruoyi.demo.vo.FiveRecordVo;
import com.ruoyi.demo.bo.FiveRecordQueryBo;
import com.ruoyi.demo.bo.FiveRecordAddBo;
import com.ruoyi.demo.bo.FiveRecordEditBo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.List;

/**
 * recordService接口
 *
 * @author ruoyi
 * @date 2021-12-08
 */
public interface IFiveRecordService extends IService<FiveRecord> {
	/**
	 * 查询单个
	 * @return
	 */
	FiveRecordVo queryById(Long id);

	/**
	 * 查询列表
	 */
	List<FiveRecordVo> queryList(FiveRecordQueryBo bo);

	/**
	 * 根据新增业务对象插入record
	 * @param bo record新增业务对象
	 * @return
	 */
	Boolean insertByAddBo(FiveRecordAddBo bo);

	Boolean insertByAdd(Long uid, BigDecimal bigDecimal,Integer coin ,Integer Event);

	/**
	 * 根据编辑业务对象修改record
	 * @param bo record编辑业务对象
	 * @return
	 */
	Boolean updateByEditBo(FiveRecordEditBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
