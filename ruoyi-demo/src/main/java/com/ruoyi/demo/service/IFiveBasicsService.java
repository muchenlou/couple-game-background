package com.ruoyi.demo.service;

import com.ruoyi.demo.domain.FiveBasics;
import com.ruoyi.demo.vo.FiveBasicsVo;
import com.ruoyi.demo.bo.FiveBasicsQueryBo;
import com.ruoyi.demo.bo.FiveBasicsAddBo;
import com.ruoyi.demo.bo.FiveBasicsEditBo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Collection;
import java.util.List;

/**
 * basicsService接口
 *
 * @author ruoyi
 * @date 2021-12-08
 */
public interface IFiveBasicsService extends IService<FiveBasics> {
	/**
	 * 查询单个
	 * @return
	 */
	FiveBasicsVo queryById(Long id);

	/**
	 * 查询列表
	 */
	List<FiveBasicsVo> queryList(FiveBasicsQueryBo bo);

	/**
	 * 根据新增业务对象插入basics
	 * @param bo basics新增业务对象
	 * @return
	 */
	Boolean insertByAddBo(FiveBasicsAddBo bo);

	/**
	 * 根据编辑业务对象修改basics
	 * @param bo basics编辑业务对象
	 * @return
	 */
	Boolean updateByEditBo(FiveBasicsEditBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
