package com.ruoyi.demo.service;

/**
 * @author Xiaofei Mu
 * @version 1.0
 * @date 2022/8/13 0:21
 */
public interface TestService {

    public String helloTest();
}
