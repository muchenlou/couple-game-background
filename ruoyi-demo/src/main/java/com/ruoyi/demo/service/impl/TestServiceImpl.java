package com.ruoyi.demo.service.impl;

import com.ruoyi.demo.service.TestService;
import org.springframework.stereotype.Service;

/**
 * @author Xiaofei Mu
 * @version 1.0
 * @date 2022/8/13 0:21
 */
@Service
public class TestServiceImpl implements TestService {
    @Override
    public String helloTest() {
        return "hello";
    }
}
