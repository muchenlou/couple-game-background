package com.ruoyi.demo.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.pagehelper.Page;
import com.ruoyi.demo.bo.FiveOrderAddBo;
import com.ruoyi.demo.bo.FiveOrderQueryBo;
import com.ruoyi.demo.bo.FiveOrderEditBo;
import com.ruoyi.demo.domain.FiveOrder;
import com.ruoyi.demo.mapper.FiveOrderMapper;
import com.ruoyi.demo.vo.FiveOrderVo;
import com.ruoyi.demo.service.IFiveOrderService;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * orderService业务层处理
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Service
public class FiveOrderServiceImpl extends ServiceImpl<FiveOrderMapper, FiveOrder> implements IFiveOrderService {

    @Override
    public FiveOrderVo queryById(Long id){
        FiveOrder db = this.baseMapper.selectById(id);
        return BeanUtil.toBean(db, FiveOrderVo.class);
    }

    @Override
    public List<FiveOrderVo> queryList(FiveOrderQueryBo bo) {
        LambdaQueryWrapper<FiveOrder> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getUid() != null, FiveOrder::getUid, bo.getUid());
        lqw.eq(StrUtil.isNotBlank(bo.getNumber()), FiveOrder::getNumber, bo.getNumber());
        lqw.eq(StrUtil.isNotBlank(bo.getCode()), FiveOrder::getCode, bo.getCode());
        lqw.eq(bo.getAmount() != null, FiveOrder::getAmount, bo.getAmount());
        return entity2Vo(this.list(lqw));
    }

    /**
    * 实体类转化成视图对象
    *
    * @param collection 实体类集合
    * @return
    */
    private List<FiveOrderVo> entity2Vo(Collection<FiveOrder> collection) {
        List<FiveOrderVo> voList = collection.stream()
                .map(any -> BeanUtil.toBean(any, FiveOrderVo.class))
                .collect(Collectors.toList());
        if (collection instanceof Page) {
            Page<FiveOrder> page = (Page<FiveOrder>)collection;
            Page<FiveOrderVo> pageVo = new Page<>();
            BeanUtil.copyProperties(page,pageVo);
            pageVo.addAll(voList);
            voList = pageVo;
        }
        return voList;
    }

    @Override
    public Boolean insertByAddBo(FiveOrderAddBo bo) {
        FiveOrder add = BeanUtil.toBean(bo, FiveOrder.class);
        validEntityBeforeSave(add);
        return this.save(add);
    }

    @Override
    public Boolean updateByEditBo(FiveOrderEditBo bo) {
        FiveOrder update = BeanUtil.toBean(bo, FiveOrder.class);
        validEntityBeforeSave(update);
        return this.updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(FiveOrder entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return this.removeByIds(ids);
    }
}
