package com.ruoyi.demo.service;

import com.ruoyi.demo.domain.FiveBank;
import com.ruoyi.demo.vo.FiveBankVo;
import com.ruoyi.demo.bo.FiveBankQueryBo;
import com.ruoyi.demo.bo.FiveBankAddBo;
import com.ruoyi.demo.bo.FiveBankEditBo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Collection;
import java.util.List;

/**
 * bankService接口
 *
 * @author ruoyi
 * @date 2021-12-08
 */
public interface IFiveBankService extends IService<FiveBank> {
	/**
	 * 查询单个
	 * @return
	 */
	FiveBankVo queryById(Long id);

	/**
	 * 查询列表
	 */
	List<FiveBankVo> queryList(FiveBankQueryBo bo);

	/**
	 * 根据新增业务对象插入bank
	 * @param bo bank新增业务对象
	 * @return
	 */
	Boolean insertByAddBo(FiveBankAddBo bo);

	/**
	 * 根据编辑业务对象修改bank
	 * @param bo bank编辑业务对象
	 * @return
	 */
	Boolean updateByEditBo(FiveBankEditBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);
}
