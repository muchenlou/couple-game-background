package com.ruoyi.demo.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.pagehelper.Page;
import com.ruoyi.demo.bo.FiveRecordAddBo;
import com.ruoyi.demo.bo.FiveRecordQueryBo;
import com.ruoyi.demo.bo.FiveRecordEditBo;
import com.ruoyi.demo.domain.FiveRecord;
import com.ruoyi.demo.mapper.FiveRecordMapper;
import com.ruoyi.demo.vo.FiveRecordVo;
import com.ruoyi.demo.service.IFiveRecordService;

import java.math.BigDecimal;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * recordService业务层处理
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Service
public class FiveRecordServiceImpl extends ServiceImpl<FiveRecordMapper, FiveRecord> implements IFiveRecordService {

    @Override
    public FiveRecordVo queryById(Long id){
        FiveRecord db = this.baseMapper.selectById(id);
        return BeanUtil.toBean(db, FiveRecordVo.class);
    }

    @Override
    public List<FiveRecordVo> queryList(FiveRecordQueryBo bo) {
        LambdaQueryWrapper<FiveRecord> lqw = Wrappers.lambdaQuery();
        lqw.eq(bo.getUid() != null, FiveRecord::getUid, bo.getUid());
        lqw.eq(bo.getAmount() != null, FiveRecord::getAmount, bo.getAmount());
        lqw.eq(bo.getCoin() != null, FiveRecord::getCoin, bo.getCoin());
        lqw.eq(bo.getEvent() != null, FiveRecord::getEvent, bo.getEvent());
        return entity2Vo(this.list(lqw));
    }



    /**
    * 实体类转化成视图对象
    *
    * @param collection 实体类集合
    * @return
    */
    private List<FiveRecordVo> entity2Vo(Collection<FiveRecord> collection) {
        List<FiveRecordVo> voList = collection.stream()
                .map(any -> BeanUtil.toBean(any, FiveRecordVo.class))
                .collect(Collectors.toList());
        if (collection instanceof Page) {
            Page<FiveRecord> page = (Page<FiveRecord>)collection;
            Page<FiveRecordVo> pageVo = new Page<>();
            BeanUtil.copyProperties(page,pageVo);
            pageVo.addAll(voList);
            voList = pageVo;
        }
        return voList;
    }

    @Override
    public Boolean insertByAddBo(FiveRecordAddBo bo) {
        FiveRecord add = BeanUtil.toBean(bo, FiveRecord.class);
        validEntityBeforeSave(add);
        return this.save(add);
    }

    @Override
    public Boolean insertByAdd(Long uid, BigDecimal bigDecimal, Integer coin, Integer event) {
        FiveRecord add=new FiveRecord();
        add.setAmount(bigDecimal);
        add.setCoin(coin);
        add.setEvent(event);
        add.setUid(uid);
        return this.save(add);
    }

    @Override
    public Boolean updateByEditBo(FiveRecordEditBo bo) {
        FiveRecord update = BeanUtil.toBean(bo, FiveRecord.class);
        validEntityBeforeSave(update);
        return this.updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(FiveRecord entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return this.removeByIds(ids);
    }
}
