package com.ruoyi.demo.service;

import com.ruoyi.demo.domain.FiveUser;
import com.ruoyi.demo.vo.FiveUserVo;
import com.ruoyi.demo.bo.FiveUserQueryBo;
import com.ruoyi.demo.bo.FiveUserAddBo;
import com.ruoyi.demo.bo.FiveUserEditBo;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.Collection;
import java.util.List;

/**
 * userService接口
 *
 * @author ruoyi
 * @date 2021-12-08
 */
public interface IFiveUserService extends IService<FiveUser> {
	/**
	 * 查询单个
	 * @return
	 */
	FiveUserVo queryById(Long id);

	/**
	 * 查询列表
	 */
	List<FiveUserVo> queryList(FiveUserQueryBo bo);

	/**
	 * 根据新增业务对象插入user
	 * @param bo user新增业务对象
	 * @return
	 */
	Boolean insertByAddBo(FiveUserAddBo bo) throws Exception;



	/**
	 * 根据编辑业务对象修改user
	 * @param bo user编辑业务对象
	 * @return
	 */
	Boolean updateByEditBo(FiveUserEditBo bo);

	/**
	 * 校验并删除数据
	 * @param ids 主键集合
	 * @param isValid 是否校验,true-删除前校验,false-不校验
	 * @return
	 */
	Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid);

	void  updatewxbyi();

	void updatewxber();
}
