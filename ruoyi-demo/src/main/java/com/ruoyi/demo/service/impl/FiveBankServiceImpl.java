package com.ruoyi.demo.service.impl;

import cn.hutool.core.bean.BeanUtil;
import cn.hutool.core.util.StrUtil;
import org.springframework.stereotype.Service;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.github.pagehelper.Page;
import com.ruoyi.demo.bo.FiveBankAddBo;
import com.ruoyi.demo.bo.FiveBankQueryBo;
import com.ruoyi.demo.bo.FiveBankEditBo;
import com.ruoyi.demo.domain.FiveBank;
import com.ruoyi.demo.mapper.FiveBankMapper;
import com.ruoyi.demo.vo.FiveBankVo;
import com.ruoyi.demo.service.IFiveBankService;

import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

/**
 * bankService业务层处理
 *
 * @author ruoyi
 * @date 2021-12-08
 */
@Service
public class FiveBankServiceImpl extends ServiceImpl<FiveBankMapper, FiveBank> implements IFiveBankService {

    @Override
    public FiveBankVo queryById(Long id){
        FiveBank db = this.baseMapper.selectById(id);
        return BeanUtil.toBean(db, FiveBankVo.class);
    }

    @Override
    public List<FiveBankVo> queryList(FiveBankQueryBo bo) {
        LambdaQueryWrapper<FiveBank> lqw = Wrappers.lambdaQuery();
        lqw.like(StrUtil.isNotBlank(bo.getName()), FiveBank::getName, bo.getName());
        lqw.eq(StrUtil.isNotBlank(bo.getIdentity()), FiveBank::getIdentity, bo.getIdentity());
        lqw.eq(StrUtil.isNotBlank(bo.getBankCard()), FiveBank::getBankCard, bo.getBankCard());
        lqw.eq(StrUtil.isNotBlank(bo.getBank()), FiveBank::getBank, bo.getBank());
        lqw.eq(StrUtil.isNotBlank(bo.getStatus()), FiveBank::getStatus, bo.getStatus());
        return entity2Vo(this.list(lqw));
    }

    /**
    * 实体类转化成视图对象
    *
    * @param collection 实体类集合
    * @return
    */
    private List<FiveBankVo> entity2Vo(Collection<FiveBank> collection) {
        List<FiveBankVo> voList = collection.stream()
                .map(any -> BeanUtil.toBean(any, FiveBankVo.class))
                .collect(Collectors.toList());
        if (collection instanceof Page) {
            Page<FiveBank> page = (Page<FiveBank>)collection;
            Page<FiveBankVo> pageVo = new Page<>();
            BeanUtil.copyProperties(page,pageVo);
            pageVo.addAll(voList);
            voList = pageVo;
        }
        return voList;
    }

    @Override
    public Boolean insertByAddBo(FiveBankAddBo bo) {
        FiveBank add = BeanUtil.toBean(bo, FiveBank.class);
        validEntityBeforeSave(add);
        return this.save(add);
    }

    @Override
    public Boolean updateByEditBo(FiveBankEditBo bo) {
        FiveBank update = BeanUtil.toBean(bo, FiveBank.class);
        validEntityBeforeSave(update);
        return this.updateById(update);
    }

    /**
     * 保存前的数据校验
     *
     * @param entity 实体类数据
     */
    private void validEntityBeforeSave(FiveBank entity){
        //TODO 做一些数据校验,如唯一约束
    }

    @Override
    public Boolean deleteWithValidByIds(Collection<Long> ids, Boolean isValid) {
        if(isValid){
            //TODO 做一些业务上的校验,判断是否需要校验
        }
        return this.removeByIds(ids);
    }
}
