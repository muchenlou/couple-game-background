package com.ruoyi.demo.service;




import com.ruoyi.demo.util.ApiToken;

import javax.servlet.http.HttpServletRequest;

public interface LoginTokenService {

    ApiToken refreshToken(Long memberId, String refreshToken);

    ApiToken creatToken(Long memberId);

    /**
     * 根据会员ID获取AccessToken的key
     */
    String getAccessTokenKeyByMemberId(Long memberId);

    /***
     * 根据会员ID获取RefreshToken的key
     */
    String getRefreshTokenKeyByMemberId(Long memberId);


    /**
     * 根据token获取AccessToken的key
     */
    String getTokenKey(String token);

    /**
     * 根据accesToken 获取token
     *
     * @param accessToken
     * @return
     */
    ApiToken getTokenByAccessToken(String accessToken);


    /**
     * 获取当前登录的用户ID
     * @param request
     * @return
     */
     Long getLoginUserId(HttpServletRequest request);
}