package com.ruoyi.demo.mapper;

import com.ruoyi.demo.domain.FiveBasics;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * basicsMapper接口
 *
 * @author ruoyi
 * @date 2021-12-08
 */
public interface FiveBasicsMapper extends BaseMapper<FiveBasics> {

}
