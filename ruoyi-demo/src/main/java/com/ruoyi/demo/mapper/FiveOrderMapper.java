package com.ruoyi.demo.mapper;

import com.ruoyi.demo.domain.FiveOrder;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * orderMapper接口
 *
 * @author ruoyi
 * @date 2021-12-08
 */
public interface FiveOrderMapper extends BaseMapper<FiveOrder> {

}
