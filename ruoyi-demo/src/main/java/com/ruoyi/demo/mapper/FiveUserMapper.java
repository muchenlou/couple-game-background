package com.ruoyi.demo.mapper;

import com.ruoyi.demo.domain.FiveUser;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * userMapper接口
 *
 * @author ruoyi
 * @date 2021-12-08
 */
public interface FiveUserMapper extends BaseMapper<FiveUser> {


    void  updatewxb();

    void updatecloudw();

    void  updatewxbyi();

    void updatewxber();

    void updatesign();

}
