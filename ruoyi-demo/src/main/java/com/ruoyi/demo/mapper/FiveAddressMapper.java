package com.ruoyi.demo.mapper;

import com.ruoyi.demo.domain.FiveAddress;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * addressMapper接口
 *
 * @author ruoyi
 * @date 2021-12-08
 */
public interface FiveAddressMapper extends BaseMapper<FiveAddress> {

}
