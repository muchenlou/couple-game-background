package com.ruoyi.demo.mapper;

import com.ruoyi.demo.domain.FiveRecord;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * recordMapper接口
 *
 * @author ruoyi
 * @date 2021-12-08
 */
public interface FiveRecordMapper extends BaseMapper<FiveRecord> {

}
