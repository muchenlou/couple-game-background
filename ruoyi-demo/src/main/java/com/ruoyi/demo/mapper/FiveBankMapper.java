package com.ruoyi.demo.mapper;

import com.ruoyi.demo.domain.FiveBank;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * bankMapper接口
 *
 * @author ruoyi
 * @date 2021-12-08
 */
public interface FiveBankMapper extends BaseMapper<FiveBank> {

}
