package com.ruoyi.demo.domain;

import com.baomidou.mybatisplus.annotation.*;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import com.ruoyi.common.annotation.Excel;

/**
 * order对象 five_order
 * 
 * @author ruoyi
 * @date 2021-12-08
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("five_order")
public class FiveOrder implements Serializable {

private static final long serialVersionUID=1L;


    /** $column.columnComment */
    @TableId(value = "id")
    private Long id;

    /** $column.columnComment */
    private Long uid;

    /** 单号 */
    private String number;

    /** 支付状态 */
    private Integer code;

    private Integer grade ;

    /** $column.columnComment */
    private Integer amount;

    /** $column.columnComment */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /** $column.columnComment */
    @TableField(fill = FieldFill.INSERT)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;

}
