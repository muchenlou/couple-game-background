package com.ruoyi.demo.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import com.ruoyi.common.annotation.Excel;

/**
 * record对象 five_record
 * 
 * @author ruoyi
 * @date 2021-12-08
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("five_record")
public class FiveRecord implements Serializable {

private static final long serialVersionUID=1L;


    /** id */
    @TableId(value = "id")
    private Long id;

    /** 用户id */
    private Long uid;

    /** 金额 */
    private BigDecimal amount;

    /** 1余额2五连3wxb4黄金 */
    private Integer  coin;

    /**  1注册  2邀请 */
    private Integer  event;

    /** 创建时间 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

}
