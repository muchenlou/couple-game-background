package com.ruoyi.demo.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import com.ruoyi.common.annotation.Excel;

/**
 * 用户对象 five_user
 * 
 * @author ruoyi
 * @date 2021-12-09
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("five_user")
public class FiveUser implements Serializable {

private static final long serialVersionUID=1L;


    /** id */
    @TableId(value = "id")
    private Long id;

    /** 手机号 */
    private String phone;

    /** 0普通会员  1.会员 */
    private Integer identity;
    /** 0普通会员  1.会员 */
    private Integer identityx;
    /** 0普通会员  1.会员 */
    private Integer identityg;

    /** 余额 */
    private BigDecimal balance;

    /** wxb */
    private Integer wxb;

    /**  云数五联币 */
    private Integer cloudw;

    /** 云数黄金 */
    private Integer cloudh;

    /** 邀请码 */
    private String code;

    /** 股权 */
    private Integer stockRight;

    /** 邀请人id */
    private Integer invitationId;

    /** 状态 */
    private Integer status;

    /** 修改时间 */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /** 创建时间 */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** 二维码 */
    private String img;

    private Integer signIn;

    private  Integer  signicon;

    private String	locations;
}
