package com.ruoyi.demo.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import com.ruoyi.common.annotation.Excel;

/**
 * bank对象 five_bank
 * 
 * @author ruoyi
 * @date 2021-12-08
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("five_bank")
public class FiveBank implements Serializable {

private static final long serialVersionUID=1L;


    /** $column.columnComment */
    @TableId(value = "id")
    private Long id;

    /** $column.columnComment */
    private String name;

    /** $column.columnComment */

    private Long uid;

    /** $column.columnComment */
    private String identity;

    /** $column.columnComment */
    private String bankCard;

    /** $column.columnComment */
    private String bank;

    /** $column.columnComment */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /** $column.columnComment */
    @TableField(fill = FieldFill.INSERT)
    private Date createTime;

    /** $column.columnComment */
    private String status;

    private String  tpwd;

}
