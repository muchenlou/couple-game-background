package com.ruoyi.demo.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import com.ruoyi.common.annotation.Excel;

/**
 * basics对象 five_basics
 * 
 * @author ruoyi
 * @date 2021-12-08
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("five_basics")
public class FiveBasics implements Serializable {

private static final long serialVersionUID=1L;


    /** $column.columnComment */
    @TableId(value = "id")
    private Integer id;

    /** $column.columnComment */
    private String conent;

    /** $column.columnComment */
    private Integer  title;

}
