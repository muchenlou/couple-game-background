package com.ruoyi.demo.domain;

import com.baomidou.mybatisplus.annotation.*;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import java.io.Serializable;
import java.util.Date;
import java.math.BigDecimal;
import com.ruoyi.common.annotation.Excel;

/**
 * address对象 five_address
 * 
 * @author ruoyi
 * @date 2021-12-08
 */
@Data
@NoArgsConstructor
@Accessors(chain = true)
@TableName("five_address")
public class FiveAddress implements Serializable {

private static final long serialVersionUID=1L;


    /** $column.columnComment */
    @TableId(value = "id")
    private Long id;

    /** $column.columnComment */
    private Long uid;

    /** $column.columnComment */
    private String address;

    /** $column.columnComment */
    private String phone;

    /** $column.columnComment */
    private String name;

    /** $column.columnComment */
    private Long status;

    /** $column.columnComment */
    @TableField(fill = FieldFill.INSERT_UPDATE)
    private Date updateTime;

    /** $column.columnComment */
    @TableField(fill = FieldFill.INSERT)
    private String createTime;

}
